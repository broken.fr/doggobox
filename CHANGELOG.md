CHANGELOG
=========

0.1.0
-----
### Features
* Add Monolog #5
* Add API-Platform #1
* Add Auth0 & entities linked #1
* Add Breed Entity #1
* Add Timestampable #3
* Add Redis session handle #4
* Add Redis cache handle #4
* Add Walk & WalkSubscription Entity #15
* Add MediaObjects 
* Add Dog Enity
* Add UserExtraData Entity
* Global initialisation of the project