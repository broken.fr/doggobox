<?php

namespace App\Service;

use Auth0\SDK\API\Authentication;
use Auth0\SDK\API\Management;
use Auth0\SDK\Auth0;
use Auth0\SDK\Configuration\SdkConfiguration;
use Psr\Http\Client\ClientInterface;

class Auth0Manager
{

    /**
     * @param SdkConfiguration $configuration
     */
    public function __construct(
        private SdkConfiguration $configuration,
        private ClientInterface $client
    ) {
        $this->configuration->setHttpClient($this->client);
    }

    /**
     * @return Management
     */
    public function management(): Management
    {
        $auth0 = new Auth0($this->configuration);

        return $auth0->management();
    }

    /**
     * @return Authentication
     */
    public function authentication(): Authentication
    {
        $auth0 = new Auth0($this->configuration);

        return $auth0->authentication();
    }
}
