<?php

namespace App\Filter;


use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Exception\InvalidArgumentException;
use ApiPlatform\Metadata\Operation;
use App\Entity\Notification;
use Doctrine\ORM\QueryBuilder;

class LastIdFilter extends AbstractFilter
{
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, Operation $operation = null, array $context = []): void
    {

        if (!$this->isPropertyEnabled($property, $resourceClass)) {
            return;
        }
        $uuidRegex = '/^[a-f\d]{8}-[a-f\d]{4}-[a-f\d]{4}-[a-f\d]{4}-[a-f\d]{12}$/i';
        $rootAlias = $queryBuilder->getRootAliases()[0];
        $queryBuilder->select($rootAlias);

        if (str_starts_with($property, 'last') && preg_match($uuidRegex, $value)) {
            /** @var Notification $offsetValue */
            $offsetValue = $this->managerRegistry->getManagerForClass($resourceClass)->getRepository($resourceClass)->findOneBy(['id' => $value]);

            if (!$offsetValue) {
                throw new InvalidArgumentException(sprintf('Invalid value provided for lastId filter for property "%s".', $property));
            }
            $parameterName = $queryNameGenerator->generateParameterName('created_at');
            $queryBuilder->andWhere(sprintf('%s.%s < :%s', $rootAlias, 'createdAt', $parameterName));
            $queryBuilder->setParameter($parameterName, $offsetValue->getCreatedAt());

            return;
        }

        $queryBuilder->andWhere(sprintf('%s = :%s', $property, $queryNameGenerator->generateParameterName($property)));
        $queryBuilder->setParameter($queryNameGenerator->generateParameterName($property), $value);
    }

    public function getDescription(string $resourceClass): array
    {
        return [
            "lastId" => [
                'property' => 'id',
                'type' => 'string',
                'required' => false,
                'description' => 'Fetches all items after the given id.'
            ]
        ];
    }
}