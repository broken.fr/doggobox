<?php

namespace App\Filter;

use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;

final class NearbyFilter extends AbstractFilter
{
    public function __construct(
        protected ManagerRegistry $managerRegistry,
        LoggerInterface $logger = null,
        protected ?array $properties = null,
        protected ?NameConverterInterface $nameConverter = null
    ) {
        parent::__construct($managerRegistry, $logger, $properties, $nameConverter);
    }

    private const COORDINATES_REGEX = '/^(?P<latitude>[-+]?(?:[1-8]?\d(?:\.\d+)?|90(?:\.0+)?)),(?P<longitude>[-+]?(?:180(\.0+)?|(?:(?:1[0-7]\d)|(?:[1-9]?\d))(?:\.\d+)?))$/';

    private const PROPERTY_NAME = 'nearby';

    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder,
                                      QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass,
                                      Operation $operation = null, array $context = []): void
    {
        if (
            !$this->isPropertyEnabled($property, $resourceClass)
        ) {
            return;
        }
        if (self::PROPERTY_NAME === $property) {
            if (!preg_match(self::COORDINATES_REGEX, $value, $matches)) {
                return;
            }
            $queryBuilder
                ->andWhere('
                    6371 * 
                    ACOS(
                        cos(radians(:latitude)) * cos(radians(o.latitude)) *
                        cos(radians(o.longitude) - radians(:longitude)) +
                        sin(radians(:latitude)) * sin(radians(o.latitude))
                    ) < :distance'
                )

                ->setParameter("latitude", $matches['latitude'])
                ->setParameter("longitude", $matches['longitude'])
            ;
        }
    }

    public function getDescription(string $resourceClass): array
    {
        if (!$this->properties) {
            return [];
        }

        $description = [];
        foreach ($this->properties as $property => $strategy) {
            $description["$property"] = [
                'property' => $property,
                'type' => 'string',
                'required' => false,
                'swagger' => [
                    'description' => 'Filter using a regex. This will appear in the Swagger documentation!',
                    'name' => 'Custom name to use in the Swagger documentation',
                    'type' => 'Will appear below the name in the Swagger documentation',
                ],
            ];
        }

        return $description;
    }
}