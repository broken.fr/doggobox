<?php

namespace App\Security\User;

use App\Entity\Permission;
use App\Entity\User;
use Auth0\Symfony\Contracts\Security\UserProviderInterface;
use Auth0\Symfony\Models\Stateful\User as StatefulUser;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserInterface as SymfonyUserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface as SymfonyUserProviderInterface;

class WebServiceUserProvider implements SymfonyUserProviderInterface, UserProviderInterface
{
    /**
     * @return User
     */
    public function getAnonymousUser(): User
    {

        $user = new User();
        $roles[] = 'IS_AUTHENTICATED_ANONYMOUSLY';
        $user
            ->setRoles($roles)
        ;
        return $user;
    }

    /**
     * @param UserInterface $user
     * @return UserInterface
     */
    public function refreshUser(UserInterface $user)
    {

        if (!$user instanceof User) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        return $this->loadUserByUsername($user->getEmail());
    }

    /**
     * @param string $class
     * @return bool
     */
    public function supportsClass($class): bool
    {
        return $class === User::class;
    }

    /**
     * @param string $identifier
     * @return UserInterface
     */
    public function loadUserByIdentifier(string $identifier): UserInterface
    {
        $identifier = \json_decode($identifier, \true);

        if ($identifier['type'] === 'stateful') {
            return new StatefulUser($identifier['data']['user']);
        }

        return $this->loadUserByJWT($identifier['data']['user']);
    }

    /**
     * @param array $jwt
     * @return User
     */
    public function loadUserByJWT(array $jwt): User
    {
        $roles = [];
        $roles[] = 'ROLE_USING_TOKEN';
        $user = new User();
        $permissions = [];
        foreach($jwt['permissions'] as $permission) {
            $obj = new Permission();
            $obj->setPermissionName($permission);
            $permissions[] = $obj;
        }
        $user
            ->setUserId($jwt['sub'])
            ->setPermissions($permissions)
            ->setRoles($roles)
        ;
        return $user;
    }

    public function loadByUserModel(\Auth0\Symfony\Models\User $user): SymfonyUserInterface
    {
        return $user;
    }
}
