<?php

namespace App\Security\Voter;

use ApiPlatform\Api\IriConverterInterface;
use App\Entity\Dog;
use App\Entity\User;
use App\Entity\UserExtraData;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;


class DogVoter extends GenericVoter
{

    const SUBJECT = 'dogs';

    const EDIT = parent::EDIT . self::SUBJECT;
    const VIEW = parent::VIEW . self::SUBJECT;
    const DELETE = parent::DELETE . self::SUBJECT;
    const CREATE = parent::CREATE . self::SUBJECT;

    public function __construct(
        private IriConverterInterface $iriConverter
    ){
    }

    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, [self::EDIT, self::VIEW, self::DELETE, self::CREATE]);
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        /** @var User $user */
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }
        $iri = $this->iriConverter->getIriFromResource(User::class, context: ['uri_variables' => ['userId' => $user->getUserIdentifier()]]);
        if (
            $subject instanceof Dog
            && $subject->getUserExtraData() instanceof UserExtraData
            && $subject->getUserExtraData()->getUser() === $iri
        ) {
            return true;
        }
        return false;
    }
}

