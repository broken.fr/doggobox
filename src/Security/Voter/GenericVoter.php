<?php

namespace App\Security\Voter;

use App\Entity\Permission;
use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class GenericVoter extends Voter
{

    const EDIT = 'update:';
    const VIEW = 'read:';
    const DELETE = 'delete:';
    const CREATE = 'create:';


    protected function supports(string $attribute, $subject): bool
    {
        return str_contains($attribute, self::EDIT)
            || str_contains($attribute, self::VIEW)
            || str_contains($attribute, self::DELETE)
            || str_contains($attribute, self::CREATE)
        ;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        /** @var User $user */
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }


        $permissions = new ArrayCollection($user->getPermissions());
        $permissions = $permissions->filter(function(Permission $permission) use ($attribute) {
            return $permission->getPermissionName() === $attribute;
        });
        return !$permissions->isEmpty();
    }
}