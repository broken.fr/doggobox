<?php

namespace App\Security\Voter;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;


class UserVoter extends GenericVoter
{
    const SUBJECT = 'users';

    const EDIT = parent::EDIT . self::SUBJECT;
    const VIEW = parent::VIEW . self::SUBJECT;
    const DELETE = parent::DELETE . self::SUBJECT;
    const CREATE = parent::CREATE . self::SUBJECT;


    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, [self::EDIT, self::VIEW, self::DELETE, self::CREATE]);
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        /** @var User $user */
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }
        if ($subject instanceof User && $subject->getUserId() === $user->getUserIdentifier()) {
            return true;
        }

        return false;
    }
}
