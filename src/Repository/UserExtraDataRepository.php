<?php

namespace App\Repository;

use App\Entity\UserExtraData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<UserExtraData>
 *
 * @method UserExtraData|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserExtraData|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserExtraData[]    findAll()
 * @method UserExtraData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserExtraDataRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserExtraData::class);
    }

    /**
     * @param UserExtraData $entity
     * @param bool $flush
     * @return void
     */
    public function add(UserExtraData $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param UserExtraData $entity
     * @param bool $flush
     * @return void
     */
    public function remove(UserExtraData $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

}
