<?php

namespace App\State\Permission;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Entity\Permission;
use App\Entity\User;
use App\Service\Auth0Manager;
use App\State\Extension\UserCollectionExtensionInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\SerializerInterface;

class PermissionCollectionProvider implements ProviderInterface
{

    private ?User $user;
    public function __construct(
        private readonly Auth0Manager                     $auth0Manager,
        private readonly SerializerInterface              $serializer,
        private readonly UserCollectionExtensionInterface $paginationExtension,
        Security                                          $security,
    ) {
        $this->user = $security->getUser();
    }
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): iterable
    {
        $resourceClass = $operation->getClass();
        $management = $this->auth0Manager->management();
        if ($id = $uriVariables['userId'] ?? $uriVariables[0][0] ?? false) {
            if ($id === "me") {
                $permissions = $this->user->getPermissions();
            } else {
                $userManager = $management->users();
                $datas = $userManager->getPermissions($id);
                if ($datas->getStatusCode() !== 200) {
                    throw new HttpException($datas->getStatusCode(), json_decode($datas->getBody()->getContents(), true)['message']);
                }
                $datas= $datas->getBody()->getContents();
                $permissions = $this->serializer->deserialize($datas, Permission::class . '[]', 'json');
            }
        }
        if ($id = $uriVariables['roleId'] ?? false) {
            $roleManager = $management->roles();
            $datas = $roleManager->getPermissions($id)->getBody()->getContents();
            $permissions = $this->serializer->deserialize($datas, Permission::class . '[]', 'json');

        }

        if (!$this->paginationExtension->isEnabled($resourceClass, $operation, $context)) {
            return $permissions;
        }
        return $this->paginationExtension->getResult($permissions, $resourceClass, $operation, $context);
    }
}
