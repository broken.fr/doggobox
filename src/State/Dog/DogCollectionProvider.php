<?php

namespace App\State\Dog;

use ApiPlatform\Api\IriConverterInterface;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Entity\User;
use App\Entity\UserExtraData;
use Doctrine\Persistence\ManagerRegistry;

class DogCollectionProvider implements ProviderInterface
{
    /**
     * @param ManagerRegistry $managerRegistry
     * @param IriConverterInterface $iriConverter
     */
    public function __construct(
        private readonly ManagerRegistry $managerRegistry,
        private readonly IriConverterInterface $iriConverter
    ) {
    }

    /**
     * @param Operation $operation
     * @param array $uriVariables
     * @param array $context
     * @return object|array|null
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|array|null
    {
        $operation = new Get(
            name: "_api_/users/{userId}{._format}_get"
        );
        $iri = $this->iriConverter->getIriFromResource(User::class, operation: $operation, context: $context);
        return $this->managerRegistry->getRepository(UserExtraData::class)->findOneBy(['user' => $iri])->getDogs();
    }
}
