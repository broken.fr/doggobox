<?php

namespace App\State\Role;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Entity\Role;
use App\Entity\User;
use App\Service\Auth0Manager;
use App\State\Extension\UserCollectionExtensionInterface;
use Auth0\SDK\Exception\ArgumentException;
use Auth0\SDK\Exception\NetworkException;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\SerializerInterface;

class RoleCollectionProvider implements ProviderInterface
{

    private ?User $user;

    /**
     * @param Auth0Manager $auth0Manager
     * @param SerializerInterface $serializer
     * @param UserCollectionExtensionInterface $paginationExtension
     * @param Security $security
     */
    public function __construct(
        private readonly Auth0Manager                     $auth0Manager,
        private readonly SerializerInterface              $serializer,
        private readonly UserCollectionExtensionInterface $paginationExtension,
        Security                                          $security,
    ) {
        $this->user = $security->getUser();
    }

    /**
     * @param Operation $operation
     * @param array $uriVariables
     * @param array $context
     * @return iterable
     * @throws ArgumentException
     * @throws NetworkException
     * @throws \Exception
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): iterable
    {
        $resourceClass = $operation->getClass();
        $management = $this->auth0Manager->management();
        $userManager = $management->users();
        if (!empty($uriVariables)) {
            $id = $uriVariables['userId'] ?? $uriVariables[0][0];
            $userId = $id;
            if ($id === "me") {
                $userId = $this->user->getUserIdentifier();
            }
            $datas = $userManager->getRoles($userId);
            if ($datas->getStatusCode() !== 200) {
                throw new HttpException($datas->getStatusCode(), json_decode($datas->getBody()->getContents(), true)['message']);
            }
            $roles = $datas->getBody()->getContents();
            $dataRoles = $this->serializer->deserialize($roles, Role::class . '[]', 'json');
            if (!$this->paginationExtension->isEnabled($resourceClass, $operation, $context)) {
                return $dataRoles;
            }
            return $this->paginationExtension->getResult($dataRoles, $resourceClass, $operation, $context);
        }
        throw new \Exception('$uriVariables should not be empty');
    }
}
