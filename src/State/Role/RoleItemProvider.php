<?php

namespace App\State\Role;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Entity\Role;
use App\Service\Auth0Manager;
use Symfony\Component\Serializer\SerializerInterface;

class RoleItemProvider implements ProviderInterface
{
    /**
     * @param Auth0Manager $auth0Manager
     * @param SerializerInterface $serializer
     */
    public function __construct(
        private readonly Auth0Manager $auth0Manager,
        private readonly SerializerInterface $serializer
    ) {
    }

    public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|array|null
    {
        $role = $this->auth0Manager->management()->roles()->get($uriVariables['id'])->getBody()->getContents();

        return $this->serializer->deserialize($role, Role::class, 'json');
    }
}
