<?php

namespace App\State\Extension;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\Pagination\ArrayPaginator;
use ApiPlatform\State\Pagination\Pagination;

final class UserPaginationExtension implements UserCollectionExtensionInterface
{
    /**
     * @param Pagination $pagination
     */
    public function __construct(private readonly Pagination $pagination)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getResult(array $collection, string $resourceClass, ?Operation $operation = null, array $context = [], ?int $totalItems = null): iterable
    {
        [, $offset, $itemPerPage] = $this->pagination->getPagination($operation, $context);

        return new ArrayPaginator($collection, $offset, $itemPerPage, $totalItems);
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabled(string $resourceClass = null, ?Operation $operation = null, array $context = []): bool
    {
        return $this->pagination->isEnabled($operation, $context);
    }
}
