<?php

namespace App\State\Extension;

use ApiPlatform\Core\DataProvider\Pagination;
use ApiPlatform\Metadata\Operation;
use App\Entity\User;

interface UserCollectionExtensionInterface
{
    /**
     * Returns the final result object.
     *
     * @param array $collection
     * @param string $resourceClass
     * @param Operation|null $operation
     * @param array $context
     *
     * @return iterable<User>
     */
    public function getResult(array $collection, string $resourceClass, ?Operation $operation = null, array $context = [], ?int $totalItems = null): iterable;

    /**
     * Tells if the extension is enabled or not.
     *
     * @param string|null $resourceClass
     * @param Operation|null $operation
     * @param array $context
     * @return bool
     */
    public function isEnabled(string $resourceClass = null, ?Operation $operation = null, array $context = []): bool;
}
