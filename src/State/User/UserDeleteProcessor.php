<?php

namespace App\State\User;

use ApiPlatform\Exception\ItemNotFoundException;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\User;
use App\Service\Auth0Manager;
use Auth0\SDK\Exception\ArgumentException;
use Auth0\SDK\Exception\NetworkException;

class UserDeleteProcessor implements ProcessorInterface
{

    /**
     * @param Auth0Manager $auth0Manager
     */
    public function __construct(private readonly Auth0Manager $auth0Manager) {

    }

    /**
     * @param $data
     * @param Operation $operation
     * @param array $uriVariables
     * @param array $context
     * @return mixed|void
     * @throws ArgumentException
     * @throws NetworkException
     */
    public function process($data, Operation $operation, array $uriVariables = [], array $context = [])
    {
        if ($data instanceof User) {
            dump($this->auth0Manager->management()->users()->delete($data->getUserId()));

        } else {
            throw new ItemNotFoundException();
        }
    }
}
