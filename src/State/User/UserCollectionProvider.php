<?php

namespace App\State\User;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Entity\User;
use App\Service\Auth0Manager;
use App\State\Extension\UserCollectionExtensionInterface;
use Doctrine\Common\Collections\Collection;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class UserCollectionProvider implements ProviderInterface
{

    private ?User $user;

    /**
     * @param Auth0Manager $auth0Manager
     * @param DenormalizerInterface $denormalizer
     * @param UserCollectionExtensionInterface $paginationExtension
     * @param Security $security
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(
        private readonly Auth0Manager                     $auth0Manager,
        private readonly DenormalizerInterface            $denormalizer,
        private readonly UserCollectionExtensionInterface $paginationExtension,
        Security                                          $security,
        private readonly ParameterBagInterface            $parameterBag
    )
    {
        $this->user = $security->getUser();
    }

    public function provide(Operation $operation, array $uriVariables = [], array $context = []): iterable
    {
        $resourceClass = $operation->getClass();
        $order = $context['filters']['order'] ?? [];
        $search = $context['filters']['search'] ?? [];
        $page = $context['filters']['page'] ?? 1;
        $itemPerPage = $context['filters']['itemsPerPage'] ?? [];

        $management = $this->auth0Manager->management();

        $params = $this->formatQuery($order, $search);
        $users = json_decode($management->users()->getAll($params, null, null, $page - 1, $itemPerPage)->getBody()->getContents(), true);

        /** @var Collection<User> $collection */
        $collection = $this->denormalizer->denormalize($users['users'], User::class . '[]');

        foreach($collection as $user){
            $user->setIsCurrentUser($this->user->getUserIdentifier() === $user->getUserId());
        }

        if (!$this->paginationExtension->isEnabled($resourceClass, $operation, $context)) {
            return $collection;
        }

        return $this->paginationExtension->getResult($collection, $resourceClass, $operation, $context, $users['total']);
    }

    /**
     * @param array $order
     * @param array $search
     * @return true[]
     */
    #[ArrayShape(['include_totals' => "bool", 'q' => "string", 'sort' => "string"])]
    public function formatQuery(array $order, array $search): array
    {
        $params = ['include_totals' => true];

        $q = 'identities.connection:"' . $this->parameterBag->get('management_auth_connection') . '"';

        if(count($order) > 1){
            throw new \InvalidArgumentException('filters.order');
        }

        if(!empty($order)){
            $orders = [ 'asc' => 1, 'desc' => -1 ];
            $field = array_key_first($order);
            $value = strtolower($order[$field]);
            if(isset($orders[$value])){
                $params['sort'] = $field . ':' . $orders[$value];
            }
        }
        if($search){
            $query = [];
            foreach($search as $field => $searchValue){
                $query[] = "$field:*$searchValue*";
            }
            $query = implode(' OR ', $query);
            $params['q'] = "$q AND ($query)";
        } else {
            $params['q'] = $q;
        }

        return $params;
    }
}
