<?php

namespace App\State\User;

use ApiPlatform\Api\IriConverterInterface;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Entity\Permission;
use App\Entity\Role;
use App\Entity\User;
use App\Entity\UserExtraData;
use App\Service\Auth0Manager;
use App\State\Permission\PermissionCollectionProvider;
use App\State\Role\RoleCollectionProvider;
use App\State\UserExtraData\UserExtraProvider;
use Auth0\SDK\Exception\ArgumentException;
use Auth0\SDK\Exception\NetworkException;
use JetBrains\PhpStorm\ArrayShape;
use Psr\Http\Client\RequestExceptionInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class FromExtraDataUserItemProvider implements ProviderInterface
{
    private ?User $user;

    /**
     * @param Auth0Manager $auth0Manager
     * @param SerializerInterface $serializer
     * @param Security $security
     * @param ProviderInterface $itemProvider
     */
    public function __construct(
        Security                                            $security,
        private ProviderInterface $doctrineItemProvider,
        private readonly IriConverterInterface $iriConverter,
    )
    {
        $this->user = $security->getUser();
    }

    /**
     * @param Operation $operation
     * @param array $uriVariables
     * @param array $context
     * @return object|array|null
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|array|null
    {
        $operation = new Get(class: UserExtraData::class, paginationEnabled: false);
        /** @var UserExtraData $userExtraData */
        $userExtraData = $this->doctrineItemProvider->provide($operation, $uriVariables, $context);
        /** @var User $user */
        $user = $this->iriConverter->getResourceFromIri($userExtraData->getUser(), $context);
        $user->setUserExtraData($userExtraData);
        return $user;
    }
}
