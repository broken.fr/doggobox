<?php

namespace App\State\User;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Entity\Permission;
use App\Entity\Role;
use App\Entity\User;
use App\Entity\UserExtraData;
use App\Service\Auth0Manager;
use App\State\Permission\PermissionCollectionProvider;
use App\State\Role\RoleCollectionProvider;
use App\State\UserExtraData\UserExtraProvider;
use Auth0\SDK\Exception\ArgumentException;
use Auth0\SDK\Exception\NetworkException;
use JetBrains\PhpStorm\ArrayShape;
use Psr\Http\Client\RequestExceptionInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\SerializerInterface;

class UserItemProvider implements ProviderInterface
{
    private ?User $user;

    /**
     * @param Auth0Manager $auth0Manager
     * @param SerializerInterface $serializer
     * @param Security $security
     * @param ProviderInterface $itemProvider
     */
    public function __construct(
        private readonly Auth0Manager                       $auth0Manager,
        private readonly SerializerInterface                $serializer,
        Security                                            $security,
        private ProviderInterface $itemProvider
    )
    {
        $this->user = $security->getUser();
    }

    /**
     * @param Operation $operation
     * @param array $uriVariables
     * @param array $context
     * @return object|array|null
     * @throws ArgumentException
     * @throws NetworkException
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|array|null
    {
        $management = $this->auth0Manager->management();
        $userId = $uriVariables['userId'];
        if ($uriVariables['userId'] === "me") {
            $userId = $this->user->getUserIdentifier();
            $isCurrentUser = true;
        }
        try {
            $data = $management->users()->get($userId)->getBody()->getContents();
            /** @var User $user */
            $user = $this->serializer->deserialize($data, User::class, 'json');
            $user->setIsCurrentUser($isCurrentUser ?? false);
            $subResources = $this->setSubResources([$uriVariables['userId']], $context);
            $user->setRoles($subResources[RoleCollectionProvider::class]);
            $user->setPermissions($subResources[PermissionCollectionProvider::class]);
            if (isset($subResources[UserExtraProvider::class])) {
                $user->setUserExtraData($subResources[UserExtraProvider::class]);
            }
        } catch (RequestExceptionInterface $e) {
            $response = json_decode($e->getResponse()->getBody()->getContents());
            throw new HttpException($e->getCode(), $response->message ?? $e->getResponse()->getReasonPhrase(), $e);
        }
        return $user;
    }

    /**
     * @param array $id
     * @param array $context
     * @return array
     */
    #[ArrayShape([
        RoleCollectionProvider::class => 'iterable',
        PermissionCollectionProvider::class => 'iterable',
        UserExtraProvider::class => UserExtraData::class|null
    ])]
    private function setSubResources(array $id, array $context): array
    {
        $subResources = [
            RoleCollectionProvider::class => [
                'operation' => GetCollection::class,
                'class' => Role::class
            ],
            PermissionCollectionProvider::class => [
                'operation' => GetCollection::class,
                'class' => Permission::class
            ]
        ];
        if ($context['operation']->getProvider() !== FromExtraDataUserItemProvider::class) {
            $subResources[UserExtraProvider::class] = [
                'operation' => Get::class,
                'class' => UserExtraData::class
            ];
        }

        $datas = [];
        foreach ($subResources as $provider => $params) {
            $operation = new $params['operation'](class: $params['class'], paginationEnabled: false, provider: $provider);
            $datas[$provider] = $this->itemProvider->provide($operation, [$id], $context);
        }

        return $datas;
    }
}
