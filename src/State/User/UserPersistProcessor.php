<?php

namespace App\State\User;

use ApiPlatform\Core\DataProvider\SubresourceDataProviderInterface;
use ApiPlatform\Exception\ResourceClassNotSupportedException;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\State\ProcessorInterface;
use ApiPlatform\State\ProviderInterface;
use App\Entity\Role;
use App\Entity\User;
use App\Service\Auth0Manager;
use App\State\Role\RoleCollectionProvider;
use Auth0\SDK\Exception\ArgumentException;
use Auth0\SDK\Exception\NetworkException;
use Http\Client\Exception\RequestException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Uid\Uuid;

class UserPersistProcessor implements ProcessorInterface
{

    /**
     * @param Auth0Manager $auth0Manager
     * @param NormalizerInterface $normalizer
     * @param DenormalizerInterface $denormalizer
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(
        private readonly Auth0Manager            $auth0Manager,
        private readonly NormalizerInterface     $normalizer,
        private readonly DenormalizerInterface   $denormalizer,
        private readonly ParameterBagInterface $parameterBag,
        private ProviderInterface $itemProvider
    ) {
    }

    /**
     * @param $data
     * @param Operation $operation
     * @param array $uriVariables
     * @param array $context
     * @return false|mixed
     * @throws ArgumentException
     * @throws ExceptionInterface
     * @throws NetworkException
     * @throws ResourceClassNotSupportedException
     */
    public function process($data, Operation $operation, array $uriVariables = [], array $context = [])
    {
        if ($data instanceof User) {
            try {
                $roles = $this->normalizer->normalize($data->getRoles(), null, [AbstractNormalizer::ATTRIBUTES => ['id']]);

                $groups = ($operation instanceof Patch) ? "user_update" : "user_create";
                $arrayData = $this->normalizer->normalize($data, null, ["groups" => $groups]);
                $arrayData['connection'] = $this->parameterBag->get('management_auth_connection');
                if ($operation instanceof Post) {
                    $dataResponse = $this->addUser($arrayData, $roles);
                } else {
                    $dataResponse = $this->updateUser($arrayData, $context, $data, $roles);
                }
            } catch (RequestException $e) {
                $response = json_decode($e->getResponse()->getBody()->getContents());
                throw new HttpException($e->getCode(), $response->message ?? $e->getResponse()->getReasonPhrase(), $e);
            }

            $arrayData = array_merge($arrayData, $dataResponse);

            return $this->denormalizer->denormalize($arrayData, User::class);
        }
        return false;
    }

    /**
     * @param array $arrayData
     * @param array $roles
     * @return array
     * @throws ArgumentException
     * @throws NetworkException
     */
    private function addUser(array $arrayData, array $roles): array
    {
        $arrayData['password'] = Uuid::v4()->toBase58();
        $arrayData['verify_email'] = false;
        $dataResponse = $this->auth0Manager->management()->users()->create($this->parameterBag->get('management_auth_connection'), $arrayData);
        if ($dataResponse->getStatusCode() !== 201) {
            throw new HttpException($dataResponse->getStatusCode(), json_decode($dataResponse->getBody()->getContents(), true)['message']);
        }
        $dataResponse = json_decode($dataResponse->getBody()->getContents(), true);
        if(!empty($roles)) $this->auth0Manager->management()->users()->addRoles($dataResponse['user_id'], array_column($roles, 'id')); // return null
        $dataResponse['roles'] = $roles;
        return $dataResponse;
    }

    /**
     * @param array $arrayData
     * @param $context
     * @param User $data
     * @param array $doggoRoles
     * @return array
     * @throws ExceptionInterface
     * @throws ResourceClassNotSupportedException
     * @throws ArgumentException
     * @throws NetworkException
     */
    private function updateUser(array $arrayData, $context, User $data, array $doggoRoles): array
    {
        if (empty($arrayData['password'])) {
            unset($arrayData['password']);
        }
        if ($context['previous_data']->getEmail() === $arrayData['email']) {
            unset($arrayData['email']);
        }
        if (!empty($arrayData['email']) && $context['previous_data']->getUsername() === $arrayData['username']) {
            unset($arrayData['username']);
        }

        $dataResponse = $this->auth0Manager->management()->users()->update($data->getUserId(), $arrayData);
        if ($dataResponse->getStatusCode() !== 200) {
            throw new HttpException($dataResponse->getStatusCode(), json_decode($dataResponse->getBody()->getContents(), true)['message']);
        }
        $operation = new GetCollection(Role::class, paginationEnabled: false, provider: RoleCollectionProvider::class);
        /** @var iterable<Role> $roles */
        $roles = $this->itemProvider->provide($operation, [$data->getUserId()], $context);
        $roles = $this->normalizer->normalize($roles);
        $toBeDeletedRoles = array_values(array_diff(array_column($roles, 'id'), array_column($doggoRoles, 'id')));
        $neededRoles = array_values(array_diff(array_column($doggoRoles, 'id'), array_column($roles, 'id')));
        if(!empty($toBeDeletedRoles)) $this->auth0Manager->management()->users()->removeRoles($data->getUserId(), $toBeDeletedRoles);
        if(!empty($neededRoles)) $this->auth0Manager->management()->users()->addRoles($dataResponse['user_id'], $neededRoles); // return null
        return json_decode($dataResponse->getBody()->getContents(), true);
    }
}
