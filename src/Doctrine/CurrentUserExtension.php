<?php

namespace App\Doctrine;

use ApiPlatform\Api\IriConverterInterface;
use ApiPlatform\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Operation;
use App\Entity\User;
use App\Entity\UserExtraData;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\SecurityBundle\Security;

class CurrentUserExtension implements QueryCollectionExtensionInterface
{

    public function __construct(private readonly Security               $security,
                                private readonly EntityManagerInterface $entityManager,
                                private readonly IriConverterInterface  $iriConverter
    ) {
    }

    public function applyToCollection(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, Operation $operation = null, array $context = []): void
    {
        $this->setParameter($queryBuilder, $context);
    }

    private function setParameter(QueryBuilder $queryBuilder, array $context = []): void
    {
        if (isset($context['filters']['nearby'])) {
            /** @var User $user */
            $user = $this->security->getUser();
            $repo = $this->entityManager->getRepository(UserExtraData::class);
            $operation = new Get(
                name: "_api_/users/{userId}{._format}_get"
            );
            $context['uri_variables'] = ['userId' => $user->getUserId()];
            $iri = $this->iriConverter->getIriFromResource(User::class, operation: $operation, context: $context);
            /** @var UserExtraData $userExtraData */
            $userExtraData = $repo->findOneBy(['user' => $iri]);
            $distance = $userExtraData->getTravelArea();
            $queryBuilder->setParameter('distance', $distance);
        }
    }
}