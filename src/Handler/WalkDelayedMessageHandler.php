<?php

namespace App\Handler;


use App\Entity\Notification;
use App\Entity\UserExtraData;
use App\Entity\Walk;
use App\Message\WalkDelayedMessage;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class WalkDelayedMessageHandler
{
    const WALK_FINISHED = 'walk_finished';
    public function __construct(private EntityManagerInterface $entityManager, private LoggerInterface $logger)
    {
    }

    /**
     * @throws \Exception
     */
    public function __invoke(WalkDelayedMessage $message): void
    {

        $givenWalk = $message->getWalk();
        $walk = $this->entityManager->getRepository(Walk::class)->find($message->getWalk()->getId());
        if ($walk) {
            $storedWalkEndTime = (new \DateTimeImmutable($walk->getWalkDate()->format('Y-m-d H:i:s')))->modify("+{$walk->getWalkDuration()} minutes");
            $givenWalkEndTime = (new \DateTimeImmutable($givenWalk->getWalkDate()->format('Y-m-d H:i:s')))->modify("+{$givenWalk->getWalkDuration()} minutes");

            if ($storedWalkEndTime == $givenWalkEndTime) {
                $notifications = [];
                $notification = new Notification();
                foreach ($walk->getWalkSubscriptions() as $walkSubscription) {
                    $notification
                        ->setRecipient($walkSubscription->getDog()->getUserExtraData())
                        ->setRelatedEntityClass(Walk::class)
                        ->setRelatedEntityId($walk->getId())
                        ->setType(self::WALK_FINISHED)
                        ->setMessage(self::WALK_FINISHED);

                    $notifications[] = clone $notification;
                }

                $this->entityManager->persist(...$notifications);
                $this->entityManager->flush();
                dump('Balade terminée id: ' . $walk->getId());
            }
        }
    }
}