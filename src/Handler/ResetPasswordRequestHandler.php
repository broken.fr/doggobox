<?php

namespace App\Handler;

use App\Message\ResetPasswordRequest;
use App\Service\Auth0Manager;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class ResetPasswordRequestHandler
{

    public function __construct(
        private Auth0Manager $auth0Manager,
        private ParameterBagInterface $container
    ) {
    }


    public function __invoke(ResetPasswordRequest $user)
    {
        $authApi = $this->auth0Manager->authentication();
        $response = $authApi->dbConnectionsChangePassword($user->getEmail(), $this->container->get('management_auth_connection'));
        return $response;
    }
}
