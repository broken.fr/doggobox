<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
final class BreedName extends Constraint
{
    public $message = 'Each word in the sentence should already exist in the database.';
}