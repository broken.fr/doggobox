<?php

namespace App\Validator\Constraints;

use App\Entity\Breed;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

final class BreedNameValidator extends ConstraintValidator
{
    public function __construct(protected EntityManagerInterface $entityManager)
    {

    }
    public function validate($value, Constraint $constraint): void
    {
        $values = explode(' ', trim($value));
        foreach($values as $word) {
            $result = $this->entityManager->getRepository(Breed::class)->createQueryBuilder('b')
                ->where('b.name LIKE :word')
                ->setParameter('word', '%'.$word.'%')
                ->getQuery()
                ->getResult()
            ;
            if (empty($result)) {
                $this->context->buildViolation($constraint->message)->addViolation();
                return;
            }
        }
    }
}