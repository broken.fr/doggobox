<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Link;
use App\State\Role\RoleCollectionProvider;
use App\State\Role\RoleItemProvider;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    operations: [
        new Get(provider: RoleItemProvider::class),
    ],
    normalizationContext: ['groups' => ['user']],
    denormalizationContext: ['groups' => ['user']],
    paginationEnabled: true
)]
#[ApiResource(
    uriTemplate: '/users/{userId}/roles.{_format}',
    operations: [new GetCollection(
        provider: RoleCollectionProvider::class
    )],
    uriVariables: ['userId' => new Link(fromClass: User::class, identifiers: ['userId'])],
    status: 200,
    normalizationContext: ['groups' => ['user']]
)]
class Role
{
    #[ApiProperty(identifier: true)]
    #[ORM\GeneratedValue]
    #[Groups(["user"])]
    private ?string $id;
    #[Groups("user")]
    private ?string $name;
    /**
     * @var Permission[]
     */
    #[Groups("user")]
    private array $permissions = [];
    /**
     * @return string|null
     */
    public function getId() : ?string
    {
        return $this->id;
    }
    /**
     * @param string|null $id
     */
    public function setId(?string $id) : void
    {
        $this->id = $id;
    }
    /**
     * @return string|null
     */
    public function getName() : ?string
    {
        return $this->name;
    }
    /**
     * @param string|null $name
     */
    public function setName(?string $name) : void
    {
        $this->name = $name;
    }
    /**
     * @return Permission[]
     */
    public function getPermissions() : array
    {
        return $this->permissions;
    }
    /**
     * @param Permission[] $permissions
     */
    public function setPermissions(array $permissions) : void
    {
        $this->permissions = $permissions;
    }
}
