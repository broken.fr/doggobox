<?php

namespace App\Entity;

use ApiPlatform\Elasticsearch\Filter\OrderFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Link;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Message\ResetPasswordRequest;
use App\State\User\FromExtraDataUserItemProvider;
use App\State\User\UserCollectionProvider;
use App\State\User\UserDeleteProcessor;
use App\State\User\UserItemProvider;
use App\State\User\UserPersistProcessor;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Uid\Uuid;

#[ApiResource(
    operations: [
        new Patch(
            normalizationContext: ['groups' => ['user_update']],
            denormalizationContext: ['groups' => ['user']],
            security: 'is_granted(constant(\'App\\\\Security\\\\Voter\\\\UserVoter::EDIT\'), object)',
            provider: UserItemProvider::class,
            processor: UserPersistProcessor::class
        ),
        new Get(
            normalizationContext: ['groups' => ['user']],
            denormalizationContext: ['groups' => ['user']],
            security: 'is_granted(constant(\'App\\\\Security\\\\Voter\\\\UserVoter::VIEW\'), object)',
            provider: UserItemProvider::class
        ),
        new Delete(
            security: 'is_granted(constant(\'App\\\\Security\\\\Voter\\\\UserVoter::DELETE\'), object)',
            provider: UserItemProvider::class,
            processor: UserDeleteProcessor::class
        ),
        new GetCollection(
            normalizationContext: ['groups' => ['user']],
            denormalizationContext: ['groups' => ['user']],
            securityPostDenormalize: 'is_granted(constant(\'App\\\\Security\\\\Voter\\\\UserVoter::VIEW\'), object)',
            provider: UserCollectionProvider::class
        ),
        new Post(
            securityPostDenormalize: 'is_granted(constant(\'App\\\\Security\\\\Voter\\\\UserVoter::CREATE\'), object)',
            processor: UserPersistProcessor::class
        ),
        new Post(
            uriTemplate: '/users/reset_password',
            status: 202,
            description: 'Send recovering password email',
            input: ResetPasswordRequest::class,
            output: false,
            messenger: 'input'
        )],
    normalizationContext: ['groups' => ['user']],
    denormalizationContext: ['groups' => ['user']],
    messenger: true,
    paginationType: 'page'
)]

#[ApiResource(
    uriTemplate: '/user_extra_datas/{id}/user.{_format}',
    operations: [new Get()],
    uriVariables: ['id' => new Link(fromClass: UserExtraData::class, identifiers: ['userId'])],
    status: 200,
    normalizationContext: ['groups' => ['user']],
    provider: FromExtraDataUserItemProvider::class
)]
#[ApiFilter(filterClass: OrderFilter::class, properties: ['userId', 'email', 'name', 'familyName'], arguments: ['orderParameterName' => 'order'])]
class User implements UserInterface, EquatableInterface
{
    #[ApiProperty(identifier: true)]
    #[ORM\GeneratedValue]
    #[SerializedName("user_id")]
    #[Groups("user")]
    private ?string $userId;

    #[ApiProperty(required: true)]
    #[Groups(["user", "user_create", "user_update"])]
    private ?string $email;

    #[SerializedName("email_verified")]
    #[Groups(["user", "user_create"])]
    private ?bool $emailVerified;

    #[Groups(["user", "user_create", "user_update"])]
    private ?string $name;

    #[ApiProperty(required: true)]
    #[Groups(["user", "user_create", "user_update"])]
    #[SerializedName("family_name")]
    private ?string $familyName = null;

    #[ApiProperty(required: true)]
    #[Groups(["user", "user_create", "user_update"])]
    #[SerializedName("username")]
    private string $username;

    /**
     * @var Role[]|string[]
     */
    #[ApiProperty(push: true)]
    #[Groups(["user"])]
    private array $roles;

    /**
     * @var Permission[]
     */
    #[ApiProperty(push: true)]
    #[Groups(["user"])]
    private array $permissions;

    #[Groups("user")]
    #[SerializedName("is_current_user")]
    private ?bool $isCurrentUser = false;

    #[Groups(["user_update", "user_create"])]
    private ?string $password = null;

    #[Groups("user")]
    private ?string $picture = null;

    #[Groups("user")]
    #[SerializedName("user_extra_data")]
    private ?UserExtraData $userExtraData;

    public function __construct()
    {
        $this->userId = Uuid::v6();
        $this->roles = [];
    }
    /**
     * @return string|null
     */
    public function getUserId() : ?string
    {
        return $this->userId;
    }

    /**
     * @param string|null $userId
     * @return User
     */
    public function setUserId(?string $userId) : User
    {
        $this->userId = $userId;
        return $this;
    }
    /**
     * @return string|null
     */
    public function getEmail() : ?string
    {
        return $this->email ?? 'anon';
    }
    /**
     * @param string|null $email
     * @return User
     */
    public function setEmail(?string $email) : User
    {
        $this->email = $email;
        return $this;
    }
    /**
     * @return bool|null
     */
    public function getEmailVerified() : ?bool
    {
        return $this->emailVerified;
    }
    /**
     * @param bool|null $emailVerified
     * @return User
     */
    public function setEmailVerified(?bool $emailVerified) : User
    {
        $this->emailVerified = $emailVerified;
        return $this;
    }
    /**
     * @return string|null
     */
    public function getName() : ?string
    {
        return $this->name;
    }
    /**
     * @param string|null $name
     * @return User
     */
    public function setName(?string $name) : User
    {
        $this->name = $name;
        return $this;
    }
    /**
     * @return string|null
     */
    public function getFamilyName() : ?string
    {
        return $this->familyName;
    }
    /**
     * @param null|string $familyName
     * @return User
     */
    public function setFamilyName(?string $familyName) : User
    {
        $this->familyName = $familyName;
        return $this;
    }
    /**
     * @return string
     */
    public function getUsername() : string
    {
        return $this->username ?? 'anon';
    }
    /**
     * @param string $username
     * @return User
     */
    public function setUsername(string $username) : User
    {
        $this->username = $username;
        return $this;
    }
    /**
     * @return Permission[]
     */
    public function getPermissions() : array
    {
        return $this->permissions;
    }
    /**
     * @param Permission[] $permissions
     * @return User
     */
    public function setPermissions(array $permissions) : User
    {
        $this->permissions = $permissions;
        return $this;
    }
    /**
     * @return Role[]|string[]
     */
    public function getRoles() : array
    {
        return $this->roles;
    }
    /**
     * @param Role[]|string[] $roles
     * @return User
     */
    public function setRoles(array $roles) : User
    {
        $this->roles = $roles;
        return $this;
    }
    /**
     * @return bool|null
     */
    public function getIsCurrentUser() : ?bool
    {
        return $this->isCurrentUser;
    }
    /**
     * @param bool|null $isCurrentUser
     * @return User
     */
    public function setIsCurrentUser(?bool $isCurrentUser) : User
    {
        $this->isCurrentUser = $isCurrentUser;
        return $this;
    }
    /**
     * @return string|null
     */
    public function getPassword() : ?string
    {
        return $this->password;
    }
    /**
     * @param string|null $password
     * @return User
     */
    public function setPassword(?string $password) : User
    {
        $this->password = $password;
        return $this;
    }
    /**
     * @inheritdoc
     */
    public function isEqualTo(UserInterface $user) : bool
    {
        if (!$user instanceof User) {
            return false;
        }
        return $this->getUserId() === $user->getUserId();
    }
    /**
     * @inheritdoc
     */
    public function eraseCredentials()
    {
    }
    /**
     * @inheritdoc
     */
    public function getUserIdentifier() : string
    {
        return $this->getUserId();
    }
    /**
     * @return UserExtraData|null
     */
    public function getUserExtraData() : ?UserExtraData
    {
        return $this->userExtraData;
    }
    /**
     * @param UserExtraData|null $userExtraData
     * @return $this
     */
    public function setUserExtraData(?UserExtraData $userExtraData) : self
    {
        $this->userExtraData = $userExtraData;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPicture(): ?string
    {
        return $this->picture;
    }

    /**
     * @param string|null $picture
     * @return User
     */
    public function setPicture(?string $picture): User
    {
        $this->picture = $picture;
        return $this;
    }

}
