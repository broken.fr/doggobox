<?php

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\Link;
use App\Repository\BreedRepository;
use App\Validator\Constraints\BreedName;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

#[ApiResource(normalizationContext: ['groups' => ['breed'], 'enable_max_depth' => true], denormalizationContext: ['groups' => ['breed']], forceEager: false, paginationPartial: false, paginationType: 'page')]
#[ORM\Entity(repositoryClass: BreedRepository::class)]
#[ApiFilter(filterClass: SearchFilter::class, properties: ['name' => 'partial'])]
#[ApiResource(uriTemplate: '/dogs/{id}/breed.{_format}', operations: [new Get()], uriVariables: ['id' => new Link(fromClass: \App\Entity\Dog::class, identifiers: ['id'])], status: 200, normalizationContext: ['groups' => ['breed']], filters: ['annotated_app_entity_breed_api_platform_core_bridge_doctrine_orm_filter_search_filter'])]
class Breed
{
    use TimestampableEntity;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["breed", "user", "user_update"])]
    private int $id;

    #[ApiProperty(required: true)]
    #[ORM\Column(name: 'breed_name', type: 'string', unique: true, nullable: false)]
    #[Groups(["breed", "user", "user_create", "user_update", "walks:view"])]
    #[BreedName]
    private string $name;

    /**
     * @var Collection<Dog>
     */
    #[ORM\OneToMany(mappedBy: 'breed', targetEntity: Dog::class, cascade: ['persist'])]
    #[Link(toProperty: 'breed')]
    #[Groups(["breed", "user", "user_create", "user_update"])]
    #[MaxDepth(1)]
    private iterable $dogs;
    public function __construct()
    {
        $this->dogs = new ArrayCollection();
    }
    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * @param int $id
     * @return Breed
     */
    public function setId(int $id) : Breed
    {
        $this->id = $id;
        return $this;
    }
    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
    /**
     * @param string $name
     * @return Breed
     */
    public function setName(string $name) : Breed
    {
        $this->name = $name;
        return $this;
    }
    /**
     * @return Dog[]
     */
    public function getDogs() : iterable
    {
        return $this->dogs;
    }
    /**
     * @param Dog[]|Collection $dogs
     * @return Breed
     */
    public function setDogs(iterable $dogs) : Breed
    {
        $this->dogs = $dogs;
        return $this;
    }
    /**
     * @param Dog $dog
     * @return $this
     */
    public function addDog(Dog $dog) : Breed
    {
        $dog->setBreed($this);
        $this->dogs->add($dog);
        return $this;
    }
    /**
     * @param Dog $dog
     * @return $this
     */
    public function removeDog(Dog $dog) : Breed
    {
        $this->dogs->removeElement($dog);
        return $this;
    }
}
