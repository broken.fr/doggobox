<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Filter\NearbyFilter;
use App\Repository\WalkRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: WalkRepository::class)]
#[ApiResource(
    operations: [
        new Get(
            normalizationContext: ['groups' => ['walks:view'], 'datetime_format' => 'Y-m-d\TH:i:s\Z'],),
        new GetCollection(
            normalizationContext: ['groups' => ['walks:list'], 'datetime_format' => 'Y-m-d\TH:i:s\Z'],
        ),
        new Delete(),
        new Post(
            normalizationContext: ['groups' => ['walks:list']],
            denormalizationContext: ['groups' => ['walks:create']]
        ),
        new Patch()
    ]
)]
#[ApiFilter(NearbyFilter::class, properties:["nearby"])]
#[ApiFilter(DateFilter::class, properties:["walkDate" => DateFilter::PARAMETER_STRICTLY_AFTER])]
#[ApiFilter(OrderFilter::class, properties: ['walkDate' => 'ASC'])]
class Walk
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    #[Groups(["dogs:list", "walks:list", "walks:view"])]
    private Uuid $id;

    #[ORM\Column]
    #[Groups(["dogs:list", "walks:list", "walks:create", "walks:view"])]
    private ?float $longitude = null;

    #[ORM\Column]
    #[Groups(["dogs:list", "walks:list", "walks:create", "walks:view"])]
    private ?float $latitude = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["dogs:list", "walks:list", "walks:create", "walks:view"])]
    private ?string $description = null;

    #[ORM\Column]
    #[Groups(["dogs:list", "walks:list", "walks:create", "walks:view"])]
    private ?int $participantsMax = null;

    #[ORM\Column(nullable: true)]
    #[Groups(["dogs:list", "walks:list", "walks:create", "walks:view"])]
    private ?int $walkDuration = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Assert\GreaterThan('now')]
    #[Groups(["dogs:list", "walks:list", "walks:create", "walks:view"])]
    private ?\DateTimeInterface $walkDate = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["dogs:list", "walks:list", "walks:create", "walks:view"])]
    private ?string $walkType = null;

    #[ORM\Column]
    #[Groups(["dogs:list", "walks:list", "walks:create", "walks:view"])]
    private ?bool $rulesAcceptance = null;

    #[ApiProperty(required: true)]
    #[Groups(["dogs:list", "walks:list", "walks:create", "walks:view"])]
    #[ORM\ManyToOne(targetEntity: UserExtraData::class, inversedBy: 'ownedWalks')]
    #[Assert\NotNull]
    private ?UserExtraData $owner = null;

    /**
     * @var Collection<WalkSubscription>
     */
    #[Assert\Count(min:1)]
    #[ORM\OneToMany(mappedBy: 'walk', targetEntity: WalkSubscription::class, cascade: ['persist'], orphanRemoval: true)]
    #[Groups(["user", "user_create", "user_update", "walks:list", "walks:create", "walks:view"])]
    private Collection $walkSubscriptions;

    public function __construct()
    {
        $this->walkSubscriptions = new ArrayCollection();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getParticipantsMax(): ?int
    {
        return $this->participantsMax;
    }

    public function setParticipantsMax(int $participantsMax): self
    {
        $this->participantsMax = $participantsMax;

        return $this;
    }

    public function getWalkDuration(): ?int
    {
        return $this->walkDuration;
    }

    public function setWalkDuration(int $walkDuration): self
    {
        $this->walkDuration = $walkDuration;

        return $this;
    }

    public function getWalkDate(): ?\DateTimeInterface
    {
        return $this->walkDate;
    }

    public function setWalkDate(\DateTimeInterface $walkDate): self
    {
        $this->walkDate = $walkDate;

        return $this;
    }

    public function getWalkType(): ?string
    {
        return $this->walkType;
    }

    public function setWalkType(string $walkType): self
    {
        $this->walkType = $walkType;

        return $this;
    }

    public function isRulesAcceptance(): ?bool
    {
        return $this->rulesAcceptance;
    }

    public function setRulesAcceptance(bool $rulesAcceptance): self
    {
        $this->rulesAcceptance = $rulesAcceptance;

        return $this;
    }

    /**
     * @return ?UserExtraData
     */
    public function getOwner(): ?UserExtraData
    {
        return $this->owner;
    }

    /**
     * @param ?UserExtraData $owner
     * @return Walk
     */
    public function setOwner(?UserExtraData $owner): Walk
    {
        $this->owner = $owner;
        return $this;
    }

    /**
     * @return Collection<WalkSubscription>
     */
    public function getWalkSubscriptions(): Collection
    {
        return $this->walkSubscriptions;
    }

    /**
     * @param Collection $walkSubscriptions
     * @return $this
     */
    public function setWalkSubscriptions(Collection $walkSubscriptions): self
    {
        $this->walkSubscriptions = $walkSubscriptions;
        return $this;
    }

    /**
     * @param WalkSubscription $walkSubscription
     * @return $this
     */
    public function addWalkSubscription(WalkSubscription $walkSubscription): self
    {
        if (!$this->walkSubscriptions->contains($walkSubscription)) {
            $this->walkSubscriptions->add($walkSubscription);
            $walkSubscription->setWalk($this);
        }
        return $this;
    }

    /**
     * @param WalkSubscription $walkSubscription
     * @return $this
     */
    public function removeWalkSubscription(WalkSubscription $walkSubscription): self
    {
        if ($this->walkSubscriptions->removeElement($walkSubscription)) {
            // set the owning side to null (unless already changed)
            if ($walkSubscription->getWalk() === $this) {
                $walkSubscription->setWalk(null);
            }
        }
        return $this;
    }

}
