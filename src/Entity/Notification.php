<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use App\Filter\LastIdFilter;
use App\Filter\UuidFilter;
use App\Repository\NotificationRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: NotificationRepository::class)]
#[ApiResource(
    mercure: 'object.getMercureOptions()',
)]
#[ApiFilter(OrderFilter::class, properties: ['createdAt' => 'DESC'])]
#[ApiFilter(SearchFilter::class, properties: ['seen' => 'exact'])]
#[ApiFilter(UuidFilter::class, properties: ['recipient' => 'exact'])]
#[ApiFilter(LastIdFilter::class, properties: ['lastId'])]
class Notification
{
    use TimestampableEntity;

    public function getMercureOptions(): array
    {
        $mercureOptions = [
            'type' => 'notification.created',
        ];

        $interval = $this->getCreatedAt()->diff($this->getUpdatedAt());
        if ($this->getId() === null || $interval->s < 1) {
            $mercureOptions['private'] = false;
            $mercureOptions['topics'] = sprintf('/api/notifications?recipient=%s', $this->getRecipient()->getId());
        }

        return $mercureOptions;
    }

    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    private Uuid $id;

    #[ORM\Column(length: 255)]
    private ?string $type = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $message = null;

    #[ORM\Column]
    private ?bool $seen = false;

    #[ORM\ManyToOne(inversedBy: 'notifications')]
    #[ORM\JoinColumn(nullable: false)]
    private ?UserExtraData $recipient = null;

    #[ORM\Column(length: 255)]
    private ?string $relatedEntityClass = null;

    #[ORM\Column]
    private string|Uuid|null $relatedEntityId = null;

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function isSeen(): ?bool
    {
        return $this->seen;
    }

    public function setSeen(bool $seen): self
    {
        $this->seen = $seen;

        return $this;
    }

    public function getRecipient(): ?UserExtraData
    {
        return $this->recipient;
    }

    public function setRecipient(?UserExtraData $recipient): self
    {
        $this->recipient = $recipient;

        return $this;
    }

    public function getRelatedEntityClass(): ?string
    {
        return $this->relatedEntityClass;
    }

    public function setRelatedEntityClass(string $relatedEntityClass): self
    {
        $this->relatedEntityClass = $relatedEntityClass;

        return $this;
    }

    public function getRelatedEntityId(): ?Uuid
    {
        if(is_string($this->relatedEntityId))
            return Uuid::fromString($this->relatedEntityId);
        return $this->relatedEntityId;
    }

    public function setRelatedEntityId(Uuid|string $relatedEntityId): self
    {
        if (is_string($relatedEntityId)) {
            $relatedEntityId = Uuid::fromString($relatedEntityId);
        }
        $this->relatedEntityId = $relatedEntityId;

        return $this;
    }
}
