<?php

namespace App\Entity;

use ApiPlatform\Api\UrlGeneratorInterface;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Link;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
#[ApiResource(
    operations: [
        new Get(),
        new GetCollection(),
        new Delete(),
        new Post(),
        new Patch()
    ],
    mercure: false
)]

#[ApiResource(
    uriTemplate: '/walks/{id}/walk_subscriptions.{_format}',
    operations: [new GetCollection()],
    uriVariables: ['id' => new Link(fromProperty: 'walkSubscriptions', fromClass: Walk::class)],
    status: 200
)]
#[ApiResource(
    uriTemplate: '/dogs/{id}/walk_subscriptions.{_format}',
    operations: [new GetCollection()],
    uriVariables: ['id' => new Link(fromProperty: 'walkSubscriptions', fromClass: Dog::class)],
    status: 200,
    normalizationContext: ['groups' => ['dogs:list']],
)]
#[UniqueEntity(fields: ["dog", "walk"])]
class WalkSubscription
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    #[Groups(["walks:view"])]
    private Uuid $id;

    #[Assert\NotNull(message: "Dog is required")]
    #[ORM\ManyToOne(targetEntity: Dog::class, inversedBy: "walkSubscriptions")]
    #[Groups(["walks:list", "walks:create", "walks:view"])]
    private ?Dog $dog;

    #[Assert\NotNull(message: "Walk is required")]
    #[ORM\ManyToOne(targetEntity: Walk::class, inversedBy: 'walkSubscriptions')]
    #[Groups(["dogs:list", "walks:list", "walks:view"])]
    private ?Walk $walk;

    #[ORM\Column(type: 'boolean')]
    #[Groups(["dogs:list", "walks:list", "walks:create", "walks:view"])]
    private bool $accepted = false;

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @param Uuid $id
     * @return WalkSubscription
     */
    public function setId(Uuid $id): WalkSubscription
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Dog|null
     */
    public function getDog(): ?Dog
    {
        return $this->dog;
    }

    /**
     * @param Dog|null $dog
     * @return WalkSubscription
     */
    public function setDog(?Dog $dog): WalkSubscription
    {
        $this->dog = $dog;
        return $this;
    }

    /**
     * @return Walk|null
     */
    public function getWalk(): ?Walk
    {
        return $this->walk;
    }

    /**
     * @param Walk|null $walk
     * @return WalkSubscription
     */
    public function setWalk(?Walk $walk): WalkSubscription
    {
        $this->walk = $walk;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAccepted(): bool
    {
        return $this->accepted;
    }

    /**
     * @param bool $accepted
     * @return WalkSubscription
     */
    public function setAccepted(bool $accepted): WalkSubscription
    {
        $this->accepted = $accepted;
        return $this;
    }
}