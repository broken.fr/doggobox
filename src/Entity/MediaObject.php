<?php
namespace App\Entity;

use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Link;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use App\Controller\CreateMediaObjectAction;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
/**
 * @Vich\Uploadable
 */
#[ApiResource(
    types: ['https://schema.org/MediaObject'],
    operations: [
        new Get(),
        new GetCollection(),
        new Delete(),
        new Post(
            controller: CreateMediaObjectAction::class,
            openapiContext: [
                'requestBody' => [
                    'content' => [
                        'multipart/form-data' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'file' => [
                                        'type' => 'string',
                                        'format' => 'binary']
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            validationContext: ['groups' => ['Default', 'user_media_create']],
            deserialize: false
        )
    ],
    normalizationContext: ['groups' => ['user_media_read', 'user']]
)]
#[ORM\Entity]
#[ApiResource(
    uriTemplate: '/dogs/{id}/image.{_format}',
    types: ['https://schema.org/MediaObject'],
    operations: [new Get()],
    uriVariables: ['id' => new Link(fromProperty: 'image', fromClass: Dog::class)],
    status: 200,
    normalizationContext: ['groups' => ['user_media_read', 'user']]
)]
class MediaObject
{
    #[ORM\Id, ORM\Column, ORM\GeneratedValue('IDENTITY')]
    private ?int $id;
    #[ApiProperty(iris: ['https://schema.org/contentUrl'])]
    #[Groups(['media_object:read', 'user', "walks:view"])]
    private ?string $contentUrl = null;
    /**
     * @Vich\UploadableField(mapping="media_object", fileNameProperty="filePath")
     */
    #[Assert\NotNull(groups: ['media_object_create'])]
    private ?File $file;
    #[ORM\Column(nullable: true)]
    #[Groups("user")]
    private ?string $filePath;
    /**
     * @return int|null
     */
    public function getId() : ?int
    {
        return $this->id;
    }
    /**
     * @param int|null $id
     * @return MediaObject
     */
    public function setId(?int $id) : MediaObject
    {
        $this->id = $id;
        return $this;
    }
    /**
     * @return string|null
     */
    public function getContentUrl() : ?string
    {
        return $this->contentUrl;
    }
    /**
     * @param string|null $contentUrl
     * @return MediaObject
     */
    public function setContentUrl(?string $contentUrl) : MediaObject
    {
        $this->contentUrl = $contentUrl;
        return $this;
    }
    /**
     * @return File|null
     */
    public function getFile() : ?File
    {
        return $this->file;
    }
    /**
     * @param File|null $file
     * @return MediaObject
     */
    public function setFile(?File $file) : MediaObject
    {
        $this->file = $file;
        return $this;
    }
    /**
     * @return string|null
     */
    public function getFilePath() : ?string
    {
        return $this->filePath;
    }
    /**
     * @param string|null $filePath
     * @return MediaObject
     */
    public function setFilePath(?string $filePath) : MediaObject
    {
        $this->filePath = $filePath;
        return $this;
    }
}