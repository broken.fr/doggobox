<?php

namespace App\Entity;

use ApiPlatform\Metadata\Link;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use App\Repository\UserExtraDataRepository;
use App\State\UserExtraData\UserExtraProvider;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    operations: [
        new Patch(
            normalizationContext: ['groups' => ['user_update']],
            denormalizationContext: ['groups' => ['user']],
            security: 'is_granted(constant(\'App\\\\Security\\\\Voter\\\\UserExtraVoter::EDIT\'), object)'
        ),
        new Get(
            normalizationContext: ['groups' => ['user']],
            denormalizationContext: ['groups' => ['user']],
            security: 'is_granted(constant(\'App\\\\Security\\\\Voter\\\\UserExtraVoter::VIEW\'), object)'
        ), new Put(
            normalizationContext: ['groups' => ['user_update']],
            denormalizationContext: ['groups' => ['user']],
            security: 'is_granted(constant(\'App\\\\Security\\\\Voter\\\\UserExtraVoter::EDIT\'), object)'
        ),
        new Delete(security: 'is_granted(constant(\'App\\\\Security\\\\Voter\\\\UserExtraVoter::DELETE\'), object)'),
        new GetCollection(
            normalizationContext: ['groups' => ['user']],
            denormalizationContext: ['groups' => ['user']],
            securityPostDenormalize: 'is_granted(constant(\'App\\\\Security\\\\Voter\\\\UserExtraVoter::VIEW\'), object)'
        ),
        new Post(
            normalizationContext: ['groups' => ['user_create']],
            denormalizationContext: ['groups' => ['user']],
            securityPostDenormalize: 'is_granted(constant(\'App\\\\Security\\\\Voter\\\\UserExtraVoter::CREATE\'), object)'
        )
    ],
    normalizationContext: ['groups' => ['user_create']],
    denormalizationContext: ['groups' => ['user']],
    forceEager: false,
    paginationPartial: false,
    paginationType: 'page'
)]
#[ORM\Entity(repositoryClass: UserExtraDataRepository::class)]
#[UniqueEntity('user')]
#[ApiResource(
    uriTemplate: '/dogs/{id}/user_extra_data.{_format}',
    operations: [new Get()],
    uriVariables: ['id' => new Link(fromProperty: 'userExtraData', fromClass: Dog::class)],
    status: 200,
)]
#[ApiResource(
    uriTemplate: '/users/{userId}/user_extra_data.{_format}',
    operations: [new Get(
        provider: UserExtraProvider::class
    )],
    uriVariables: ['userId' => new Link(fromClass: User::class, identifiers: ['userId'])],
    status: 200,
    normalizationContext: ['groups' => ['user']],
)]
class UserExtraData
{
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    #[Groups(["user", "user_update", "walks:view"])]
    private Uuid $id;

    #[ApiProperty(required: true)]
    #[ORM\Column(type: Types::INTEGER, nullable: false)]
    #[Groups(["user", "user_create", "user_update"])]
    #[SerializedName("travel_area")]
    #[Assert\NotNull]
    #[Assert\PositiveOrZero]
    private int $travelArea;

    #[ApiProperty(required: true)]
    #[ORM\Column(type: Types::INTEGER, nullable: false)]
    #[Groups(["user", "user_create", "user_update"])]
    #[SerializedName("positive_way")]
    #[Assert\NotNull]
    #[Assert\Range(min: 0, max: 10)]
    private int $positiveWay;

    #[ApiProperty(required: true)]
    #[ORM\Column(type: Types::INTEGER, nullable: false)]
    #[Groups(["user", "user_create", "user_update"])]
    #[SerializedName("natural_way")]
    #[Assert\NotNull]
    #[Assert\Range(min: 0, max: 10)]
    private int $naturalWay;

    #[ApiProperty(required: true)]
    #[ORM\Column(type: Types::INTEGER, nullable: false)]
    #[Groups(["user", "user_create", "user_update"])]
    #[SerializedName("traditional_way")]
    #[Assert\NotNull]
    #[Assert\Range(min: 0, max: 10)]
    private int $traditionalWay;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["user", "user_create", "user_update"])]
    private ?string $resume;

    /**
     * @var Collection<Dog>
     */
    #[ORM\OneToMany(mappedBy: 'userExtraData', targetEntity: Dog::class, orphanRemoval: true)]
    #[ApiProperty(push: true)]
    #[Groups(["user", "user_create", "user_update", "walks:view"])]
    private Collection $dogs;

    #[ApiProperty(required: true)]
    #[ORM\Column(name: 'user', type: 'string', unique: true)]
    #[Groups(["user", "walks:view"])]
    private string $user;

    /**
     * @var Collection<Walk>
     */
    #[ORM\OneToMany(mappedBy: 'owner', targetEntity: Walk::class, orphanRemoval: true)]
    #[Groups(["user", "user_create", "user_update"])]
    private Collection $ownedWalks;

    #[ORM\OneToMany(mappedBy: 'recipient', targetEntity: Notification::class, orphanRemoval: true)]
    private Collection $notifications;

    #[ORM\OneToMany(mappedBy: 'reviewer', targetEntity: Review::class)]
    private Collection $reviews;

    public function __construct()
    {
        $this->dogs = new ArrayCollection();
        $this->ownedWalks = new ArrayCollection();
        $this->notifications = new ArrayCollection();
        $this->reviews = new ArrayCollection();
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return integer
     */
    public function getTravelArea(): int
    {
        return $this->travelArea;
    }

    /**
     * @param integer $travelArea
     * @return self
     */
    public function setTravelArea(int $travelArea): self
    {
        $this->travelArea = $travelArea;
        return $this;
    }

    /**
     * @return integer
     */
    public function getPositiveWay(): int
    {
        return $this->positiveWay;
    }

    /**
     * @param integer $positiveWay
     * @return self
     */
    public function setPositiveWay(int $positiveWay): self
    {
        $this->positiveWay = $positiveWay;
        return $this;
    }

    /**
     * @return integer
     */
    public function getNaturalWay(): int
    {
        return $this->naturalWay;
    }

    /**
     * @param integer $naturalWay
     * @return self
     */
    public function setNaturalWay(int $naturalWay): self
    {
        $this->naturalWay = $naturalWay;
        return $this;
    }

    /**
     * @return integer
     */
    public function getTraditionalWay(): int
    {
        return $this->traditionalWay;
    }

    /**
     * @param integer $traditionalWay
     * @return self
     */
    public function setTraditionalWay(int $traditionalWay): self
    {
        $this->traditionalWay = $traditionalWay;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getResume(): ?string
    {
        return $this->resume;
    }

    /**
     * @param string|null $resume
     * @return self
     */
    public function setResume(?string $resume): self
    {
        $this->resume = $resume;
        return $this;
    }

    /**
     * @param Collection<int, Dog> $dogs
     * @return UserExtraData
     */
    public function setDogs(Collection $dogs): UserExtraData
    {
        $this->dogs = $dogs;
        return $this;
    }

    /**
     * @return Collection<int, Dog>
     */
    public function getDogs(): Collection
    {
        return $this->dogs;
    }

    /**
     * @param Dog $dog
     * @return self
     */
    public function addDog(Dog $dog): self
    {
        if (!$this->dogs->contains($dog)) {
            $this->dogs->add($dog);
            $dog->setUserExtraData($this);
        }
        return $this;
    }

    /**
     * @param Dog $dog
     * @return self
     */
    public function removeDog(Dog $dog): self
    {
        if ($this->dogs->removeElement($dog)) {
            // set the owning side to null (unless already changed)
            if ($dog->getUserExtraData() === $this) {
                $dog->setUserExtraData(null);
            }
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getUser(): string
    {
        return $this->user;
    }

    /**
     * @param string $user
     * @return self
     */
    public function setUser(string $user): self
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getOwnedWalks(): Collection
    {
        return $this->ownedWalks;
    }

    /**
     * @param Collection $ownedWalks
     * @return UserExtraData
     */
    public function setOwnedWalks(Collection $ownedWalks): UserExtraData
    {
        $this->ownedWalks = $ownedWalks;
        return $this;
    }

    /**
     * @param Walk $ownedWalk
     * @return $this
     */
    public function addOwnedWalk(Walk $ownedWalk): self
    {
        if (!$this->ownedWalks->contains($ownedWalk)) {
            $this->ownedWalks->add($ownedWalk);
            $ownedWalk->setOwner($this);
        }
        return $this;
    }

    /**
     * @param Walk $ownedWalk
     * @return $this
     */
    public function removeOwnedWalk(Walk $ownedWalk): self
    {
        if ($this->ownedWalks->removeElement($ownedWalk)) {
            // set the owning side to null (unless already changed)
            if ($ownedWalk->getOwner() === $this) {
                $ownedWalk->setOwner(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, Notification>
     */
    public function getNotifications(): Collection
    {
        return $this->notifications;
    }

    public function addRelatedEntityClass(Notification $notification): self
    {
        if (!$this->notifications->contains($notification)) {
            $this->notifications->add($notification);
            $notification->setRecipient($this);
        }

        return $this;
    }

    public function removeRelatedEntityClass(Notification $notification): self
    {
        if ($this->notifications->removeElement($notification)) {
            // set the owning side to null (unless already changed)
            if ($notification->getRecipient() === $this) {
                $notification->setRecipient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Review>
     */
    public function getReviews(): Collection
    {
        return $this->reviews;
    }

    public function addReview(Review $review): self
    {
        if (!$this->reviews->contains($review)) {
            $this->reviews->add($review);
            $review->setReviewer($this);
        }

        return $this;
    }

    public function removeReview(Review $review): self
    {
        if ($this->reviews->removeElement($review)) {
            // set the owning side to null (unless already changed)
            if ($review->getReviewer() === $this) {
                $review->setReviewer(null);
            }
        }

        return $this;
    }
}
