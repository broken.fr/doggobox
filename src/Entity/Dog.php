<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Link;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Repository\DogRepository;
use App\State\Dog\DogCollectionProvider;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    operations: [
        new Patch(security: 'is_granted(constant(\'App\\\\Security\\\\Voter\\\\DogVoter::EDIT\'), object)'),
        new Get(security: 'is_granted(constant(\'App\\\\Security\\\\Voter\\\\DogVoter::VIEW\'), object)'),
        new Put(security: 'is_granted(constant(\'App\\\\Security\\\\Voter\\\\DogVoter::EDIT\'), object)'),
        new Delete(security: 'is_granted(constant(\'App\\\\Security\\\\Voter\\\\DogVoter::DELETE\'), object)'),
        new GetCollection(security: 'is_granted(constant(\'App\\\\Security\\\\Voter\\\\DogVoter::VIEW\'), object)'),
        new Post(securityPostDenormalize: 'is_granted(constant(\'App\\\\Security\\\\Voter\\\\DogVoter::CREATE\'), object)'),
        new GetCollection(name: 'api_users_user_extra_data_dogs_get_subresource')
    ],
    normalizationContext: ['groups' => ['user']],
    denormalizationContext: ['groups' => ['user']],
    forceEager: false,
    paginationPartial: false,
    paginationType: 'page'
)]
#[ORM\Entity(repositoryClass: DogRepository::class)]
#[ApiResource(
    uriTemplate: '/breeds/{id}/dogs.{_format}',
    operations: [new GetCollection()],
    uriVariables: ['id' => new Link(fromProperty: 'dogs', fromClass: Breed::class)],
    status: 200,
    normalizationContext: ['groups' => ['user'], 'enable_max_depth' => true]
)]
#[ApiResource(
    uriTemplate: '/users/{userId}/user_extra_data/dogs.{_format}',
    operations: [new GetCollection()],
    uriVariables: [
        'userId' => new Link(fromClass: User::class, identifiers: ['userId']),
    ],
    status: 200,
    normalizationContext: ['groups' => ['user']],
    provider: DogCollectionProvider::class
)]
#[ApiResource(
    uriTemplate: '/user_extra_datas/{id}/dogs.{_format}',
    operations: [new GetCollection()],
    uriVariables: ['id' => new Link(fromProperty: 'dogs', fromClass: UserExtraData::class)],
    status: 200,
    normalizationContext: ['enable_max_depth' => true]
)]
class Dog
{
    const GENDER_FEMALE = 'female';
    const GENDER_MALE = 'male';
    const CATEGORY_1 = 'category_1';
    const CATEGORY_2 = 'category_2';
    const CATEGORY_3 = 'category_3';
    use TimestampableEntity;

    #[ApiProperty(push: true, iris: ['https://schema.org/image'])]
    #[ORM\ManyToOne(targetEntity: MediaObject::class)]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(["user", "walks:view", "user_update"])]
    public ?MediaObject $image;

    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    #[Groups(["user", "user_update", "walks:view"])]
    private Uuid $id;

    #[ApiProperty(required: true)]
    #[ORM\Column(name: 'dog_name', type: 'string', nullable: false)]
    #[Groups(["user", "walks:view", "user_update"])]
    #[Assert\NotNull]
    private string $name;

    #[ApiProperty(required: true, push: true)]
    #[ORM\ManyToOne(targetEntity: Breed::class, cascade: ["all"], inversedBy: "dogs")]
    #[Groups(["user", "walks:view", "user_update"])]
    #[Assert\NotNull]
    #[Assert\Valid(traverse: true)]
    #[MaxDepth(1)]
    private Breed $breed;

    #[ApiProperty(required: true, push: true)]
    #[ORM\ManyToOne(targetEntity: UserExtraData::class, inversedBy: 'dogs')]
    #[Groups(["user", "walks:view"])]
    #[Assert\NotNull]
    private UserExtraData $userExtraData;
    #[ApiProperty(required: true)]
    #[ORM\Column(name: 'dog_sterilized', type: 'boolean', nullable: false)]
    #[Groups(["user", "user_update"])]
    #[Assert\NotNull]
    private bool $sterilized;
    #[ApiProperty(required: true)]
    #[ORM\Column(name: 'dog_gender', type: 'string', nullable: false)]
    #[Groups(["user", "walks:view", "user_update"])]
    #[Assert\Choice([self::GENDER_FEMALE, self::GENDER_MALE])]
    #[Assert\NotNull]
    private string $gender;
    #[ApiProperty(required: true)]
    #[ORM\Column(name: 'dog_birthdate', type: 'datetime', nullable: false)]
    #[Groups(["user", "walks:view", "user_update"])]
    #[Assert\NotNull]
    #[Assert\LessThan('now')]
    private DateTime $birthdate;
    #[ApiProperty(required: true)]
    #[ORM\Column(name: 'dog_category', type: 'string', nullable: false)]
    #[Groups(["user", "user_update"])]
    #[Assert\Choice([self::CATEGORY_1, self::CATEGORY_2, self::CATEGORY_3])]
    private ?string $category;
    #[ApiProperty(required: true)]
    #[ORM\Column(name: 'dog_amity_males', type: 'integer', nullable: false)]
    #[Groups("user")]
    #[Assert\NotNull]
    #[Assert\Range(min: 0, max: 5)]
    private int $amityMales;
    #[ApiProperty(required: true)]
    #[ORM\Column(name: 'dog_amity_females', type: 'integer', nullable: false)]
    #[Groups("user")]
    #[Assert\NotNull]
    #[Assert\Range(min: 0, max: 5)]
    private int $amityFemales;
    #[ApiProperty(required: true)]
    #[ORM\Column(name: 'dog_amityPuppies', type: 'integer', nullable: false)]
    #[Groups("user")]
    #[Assert\NotNull]
    #[Assert\Range(min: 0, max: 5)]
    private int $amityPuppies;

    #[ApiProperty(required: true)]
    #[ORM\Column(name: 'dog_playfullness', type: 'integer', nullable: false)]
    #[Groups("user")]
    #[Assert\NotNull]
    #[Assert\Range(min: 0, max: 5)]
    private int $playfullness;

    #[ApiProperty(required: true)]
    #[ORM\Column(name: 'dog_sharing', type: 'integer', nullable: false)]
    #[Groups("user")]
    #[Assert\NotNull]
    #[Assert\Range(min: 0, max: 5)]
    private int $sharing;

    #[ApiProperty(required: true)]
    #[ORM\Column(name: 'dog_runner', type: 'integer', nullable: false)]
    #[Groups("user")]
    #[Assert\NotNull]
    #[Assert\Range(min: 0, max: 5)]
    private int $runner;

    #[ApiProperty(required: true)]
    #[ORM\Column(name: 'dog_return_response', type: 'integer', nullable: false)]
    #[Groups("user")]
    #[Assert\NotNull]
    #[Assert\Range(min: 0, max: 5)]
    private int $returnResponse;

    #[ApiProperty(required: true)]
    #[ORM\Column(name: 'dog_stop_response', type: 'integer', nullable: false)]
    #[Groups("user")]
    #[Assert\NotNull]
    #[Assert\Range(min: 0, max: 5)]
    private int $stopResponse;

    #[ORM\Column(name: 'dog_resume', type: 'text', nullable: true)]
    #[Groups("user")]
    private ?string $resume;

    #[ApiProperty(required: true)]
    #[ORM\Column(name: 'dog_adventurous_temper', type: 'boolean', nullable: false)]
    #[Groups("user")]
    #[Assert\NotNull]
    private bool $adventurousTemper;

    #[ApiProperty(required: true)]
    #[ORM\Column(name: 'dog_grumpy_temper', type: 'boolean', nullable: false)]
    #[Groups("user")]
    #[Assert\NotNull]
    private bool $grumpyTemper;

    #[ApiProperty(required: true)]
    #[ORM\Column(name: 'dog_peaceful_temper', type: 'boolean', nullable: false)]
    #[Groups("user")]
    #[Assert\NotNull]
    private bool $peacefulTemper;

    #[ApiProperty(required: true)]
    #[ORM\Column(name: 'dog_brawler_temper', type: 'boolean', nullable: false)]
    #[Groups("user")]
    #[Assert\NotNull]
    private bool $brawlerTemper;

    #[ApiProperty(required: true)]
    #[ORM\Column(name: 'dog_independent_temper', type: 'boolean', nullable: false)]
    #[Groups("user")]
    #[Assert\NotNull]
    private bool $independentTemper;

    #[ApiProperty(required: true)]
    #[ORM\Column(name: 'dog_brutal_temper', type: 'boolean', nullable: false)]
    #[Groups("user")]
    #[Assert\NotNull]
    private bool $brutalTemper;

    #[ApiProperty(required: true)]
    #[ORM\Column(name: 'dog_mild_temper', type: 'boolean', nullable: false)]
    #[Groups("user")]
    #[Assert\NotNull]
    private bool $mildTemper;

    #[ApiProperty(required: true)]
    #[ORM\Column(name: 'dog_shy_temper', type: 'boolean', nullable: false)]
    #[Groups("user")]
    #[Assert\NotNull]
    private bool $shyTemper;

    #[ApiProperty(required: true)]
    #[ORM\Column(name: 'dog_sniffer', type: 'integer', nullable: false)]
    #[Groups("user")]
    #[Assert\NotNull]
    #[Assert\Range(min: 0, max: 5)]
    private int $sniffer;

    #[ApiProperty(required: true)]
    #[ORM\Column(name: 'dog_swimmer', type: 'integer', nullable: false)]
    #[Groups("user")]
    #[Assert\NotNull]
    #[Assert\Range(min: 0, max: 5)]
    private int $swimmer;

    #[ApiProperty(required: true)]
    #[ORM\Column(name: 'dog_fighter', type: 'integer', nullable: false)]
    #[Groups("user")]
    #[Assert\NotNull]
    #[Assert\Range(min: 0, max: 5)]
    private int $fighter;

    #[ApiProperty(required: true)]
    #[ORM\Column(name: 'dog_energy_level', type: 'integer', nullable: false)]
    #[Groups("user")]
    #[Assert\NotNull]
    #[Assert\Range(min: 0, max: 5)]
    private int $energyLevel;

    /**
     * @var Collection<WalkSubscription>
     */
    #[ApiProperty(push: true)]
    #[ORM\OneToMany(mappedBy: 'dog', targetEntity: WalkSubscription::class, orphanRemoval: true)]
    #[Groups(["user", "user_create", "user_update"])]
    private Collection $walkSubscriptions;

    public function __construct()
    {
        $this->walkSubscriptions = new ArrayCollection();
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @param Uuid $id
     *
     * @return Dog
     */
    public function setId(Uuid $id): Dog
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Dog
     */
    public function setName(string $name): Dog
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Breed
     */
    public function getBreed(): Breed
    {
        return $this->breed;
    }

    /**
     * @param Breed $breed
     *
     * @return Dog
     */
    public function setBreed(Breed $breed): Dog
    {
        $this->breed = $breed;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSterilized(): bool
    {
        return $this->sterilized;
    }

    /**
     * @param bool $sterilized
     *
     * @return Dog
     */
    public function setSterilized(bool $sterilized): Dog
    {
        $this->sterilized = $sterilized;
        return $this;
    }

    /**
     * @return string
     */
    public function getGender(): string
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     *
     * @return Dog
     */
    public function setGender(string $gender): Dog
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getBirthdate(): DateTime
    {
        return $this->birthdate;
    }

    /**
     * @param DateTime $birthdate
     *
     * @return Dog
     */
    public function setBirthdate(DateTime $birthdate): Dog
    {
        $this->birthdate = $birthdate;
        return $this;
    }

    /**
     * @return ?string
     */
    public function getCategory(): ?string
    {
        return $this->category;
    }

    /**
     * @param ?string $category
     *
     * @return Dog
     */
    public function setCategory(?string $category): Dog
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmityMales(): int
    {
        return $this->amityMales;
    }

    /**
     * @param int $amityMales
     *
     * @return Dog
     */
    public function setAmityMales(int $amityMales): Dog
    {
        $this->amityMales = $amityMales;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmityFemales(): int
    {
        return $this->amityFemales;
    }

    /**
     * @param int $amityFemales
     *
     * @return Dog
     */
    public function setAmityFemales(int $amityFemales): Dog
    {
        $this->amityFemales = $amityFemales;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmityPuppies(): int
    {
        return $this->amityPuppies;
    }

    /**
     * @param int $amityPuppies
     *
     * @return Dog
     */
    public function setAmityPuppies(int $amityPuppies): Dog
    {
        $this->amityPuppies = $amityPuppies;
        return $this;
    }

    /**
     * @return int
     */
    public function getPlayfullness(): int
    {
        return $this->playfullness;
    }

    /**
     * @param int $playfullness
     *
     * @return Dog
     */
    public function setPlayfullness(int $playfullness): Dog
    {
        $this->playfullness = $playfullness;
        return $this;
    }

    /**
     * @return int
     */
    public function getSharing(): int
    {
        return $this->sharing;
    }

    /**
     * @param int $sharing
     *
     * @return Dog
     */
    public function setSharing(int $sharing): Dog
    {
        $this->sharing = $sharing;
        return $this;
    }

    /**
     * @return int
     */
    public function getRunner(): int
    {
        return $this->runner;
    }

    /**
     * @param int $runner
     *
     * @return Dog
     */
    public function setRunner(int $runner): Dog
    {
        $this->runner = $runner;
        return $this;
    }

    /**
     * @return int
     */
    public function getReturnResponse(): int
    {
        return $this->returnResponse;
    }

    /**
     * @param int $returnResponse
     *
     * @return Dog
     */
    public function setReturnResponse(int $returnResponse): Dog
    {
        $this->returnResponse = $returnResponse;
        return $this;
    }

    /**
     * @return int
     */
    public function getStopResponse(): int
    {
        return $this->stopResponse;
    }

    /**
     * @param int $stopResponse
     *
     * @return Dog
     */
    public function setStopResponse(int $stopResponse): Dog
    {
        $this->stopResponse = $stopResponse;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getResume(): ?string
    {
        return $this->resume;
    }

    /**
     * @return UserExtraData|null
     */
    public function getUserExtraData(): ?UserExtraData
    {
        return $this->userExtraData ?? null;
    }

    /**
     * @param UserExtraData $userExtraData
     * @return $this
     */
    public function setUserExtraData(UserExtraData $userExtraData): self
    {
        $this->userExtraData = $userExtraData;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAdventurousTemper(): bool
    {
        return $this->adventurousTemper;
    }

    /**
     * @param bool $adventurousTemper
     * @return Dog
     */
    public function setAdventurousTemper(bool $adventurousTemper): Dog
    {
        $this->adventurousTemper = $adventurousTemper;
        return $this;
    }

    /**
     * @return bool
     */
    public function isGrumpyTemper(): bool
    {
        return $this->grumpyTemper;
    }

    /**
     * @param bool $grumpyTemper
     * @return Dog
     */
    public function setGrumpyTemper(bool $grumpyTemper): Dog
    {
        $this->grumpyTemper = $grumpyTemper;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPeacefulTemper(): bool
    {
        return $this->peacefulTemper;
    }

    /**
     * @param bool $peacefulTemper
     * @return Dog
     */
    public function setPeacefulTemper(bool $peacefulTemper): Dog
    {
        $this->peacefulTemper = $peacefulTemper;
        return $this;
    }

    /**
     * @return bool
     */
    public function isBrawlerTemper(): bool
    {
        return $this->brawlerTemper;
    }

    /**
     * @param bool $brawlerTemper
     * @return Dog
     */
    public function setBrawlerTemper(bool $brawlerTemper): Dog
    {
        $this->brawlerTemper = $brawlerTemper;
        return $this;
    }

    /**
     * @return bool
     */
    public function isIndependentTemper(): bool
    {
        return $this->independentTemper;
    }

    /**
     * @param bool $independentTemper
     * @return Dog
     */
    public function setIndependentTemper(bool $independentTemper): Dog
    {
        $this->independentTemper = $independentTemper;
        return $this;
    }

    /**
     * @return bool
     */
    public function isBrutalTemper(): bool
    {
        return $this->brutalTemper;
    }

    /**
     * @param bool $brutalTemper
     * @return Dog
     */
    public function setBrutalTemper(bool $brutalTemper): Dog
    {
        $this->brutalTemper = $brutalTemper;
        return $this;
    }

    /**
     * @return bool
     */
    public function isMildTemper(): bool
    {
        return $this->mildTemper;
    }

    /**
     * @param bool $mildTemper
     * @return Dog
     */
    public function setMildTemper(bool $mildTemper): Dog
    {
        $this->mildTemper = $mildTemper;
        return $this;
    }

    /**
     * @return bool
     */
    public function isShyTemper(): bool
    {
        return $this->shyTemper;
    }

    /**
     * @param bool $shyTemper
     * @return Dog
     */
    public function setShyTemper(bool $shyTemper): Dog
    {
        $this->shyTemper = $shyTemper;
        return $this;
    }

    /**
     * @return int
     */
    public function getSniffer(): int
    {
        return $this->sniffer;
    }

    /**
     * @param int $sniffer
     * @return Dog
     */
    public function setSniffer(int $sniffer): Dog
    {
        $this->sniffer = $sniffer;
        return $this;
    }

    /**
     * @return int
     */
    public function getSwimmer(): int
    {
        return $this->swimmer;
    }

    /**
     * @param int $swimmer
     * @return Dog
     */
    public function setSwimmer(int $swimmer): Dog
    {
        $this->swimmer = $swimmer;
        return $this;
    }

    /**
     * @return int
     */
    public function getFighter(): int
    {
        return $this->fighter;
    }

    /**
     * @param int $fighter
     * @return Dog
     */
    public function setFighter(int $fighter): Dog
    {
        $this->fighter = $fighter;
        return $this;
    }

    /**
     * @return int
     */
    public function getEnergyLevel(): int
    {
        return $this->energyLevel;
    }

    /**
     * @param int $energyLevel
     * @return Dog
     */
    public function setEnergyLevel(int $energyLevel): Dog
    {
        $this->energyLevel = $energyLevel;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getWalkSubscriptions(): Collection
    {
        return $this->walkSubscriptions;
    }

    /**
     * @param Collection $walkSubscriptions
     * @return $this
     */
    public function setWalkSubscriptions(Collection $walkSubscriptions): self
    {
        $this->walkSubscriptions = $walkSubscriptions;
        return $this;
    }

    /**
     * @param WalkSubscription $walkSubscription
     * @return $this
     */
    public function addWalkSubscription(WalkSubscription $walkSubscription): self
    {
        if (!$this->walkSubscriptions->contains($walkSubscription)) {
            $this->walkSubscriptions->add($walkSubscription);
            $walkSubscription->setDog($this);
        }
        return $this;
    }

    /**
     * @param WalkSubscription $walkSubscription
     * @return $this
     */
    public function removeWalkSubscription(WalkSubscription $walkSubscription): self
    {
        if ($this->walkSubscriptions->removeElement($walkSubscription)) {
            // set the owning side to null (unless already changed)
            if ($walkSubscription->getDog() === $this) {
                $walkSubscription->setDog(null);
            }
        }
        return $this;
    }
}
