<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\ReviewRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ReviewRepository::class)]
#[ORM\Index(columns: ["related_entity_id"], name: "related_entity_id_idx")]
#[ORM\Index(columns: ["related_entity_class"], name: "related_entity_class_idx")]
#[ORM\Index(columns: ["reviewed_entity_id"], name: "reviewed_entity_id_idx")]
#[ORM\Index(columns: ["reviewed_entity_class"], name: "reviewed_entity_class_idx")]
#[ORM\Index(columns: ["type"], name: "type")]
#[ORM\UniqueConstraint(name: "review_by_event_by_reviewer_uniq", columns: ["reviewer_id", "reviewed_entity_class", "reviewed_entity_id", "origin_entity_class", "origin_entity_id"])]
#[ApiResource]
class Review
{
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    private Uuid $id;

    #[ORM\Column]
    #[Assert\Length(min: 0, max: 5)]
    private ?int $score = null;

    #[ORM\Column(length: 255)]
    private ?string $type = null;

    #[ORM\ManyToOne(inversedBy: 'reviews')]
    private ?UserExtraData $reviewer = null;

    #[ORM\Column(length: 255)]
    private ?string $relatedEntityClass = null;

    #[ORM\Column(type: 'uuid')]
    private ?Uuid $relatedEntityId = null;

    #[ORM\Column(length: 255)]
    private ?string $reviewedEntityClass = null;

    #[ORM\Column(type: 'uuid')]
    private ?Uuid $reviewedEntityId = null;

    #[ORM\Column(length: 255)]
    private ?string $originEntityClass = null;

    #[ORM\Column(type: 'uuid')]
    private ?Uuid $originEntityId = null;

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getReviewer(): ?UserExtraData
    {
        return $this->reviewer;
    }

    public function setReviewer(?UserExtraData $reviewer): self
    {
        $this->reviewer = $reviewer;

        return $this;
    }

    public function getRelatedEntityClass(): ?string
    {
        return $this->relatedEntityClass;
    }

    public function setRelatedEntityClass(string $relatedEntityClass): self
    {
        $this->relatedEntityClass = $relatedEntityClass;

        return $this;
    }

    public function getRelatedEntityId(): ?Uuid
    {
        return $this->relatedEntityId;
    }

    public function setRelatedEntityId(Uuid $relatedEntityId): self
    {
        $this->relatedEntityId = $relatedEntityId;

        return $this;
    }

    public function getReviewedEntityClass(): ?string
    {
        return $this->reviewedEntityClass;
    }

    public function setReviewedEntityClass(string $reviewedEntityClass): self
    {
        $this->reviewedEntityClass = $reviewedEntityClass;

        return $this;
    }

    public function getReviewedEntityId(): ?Uuid
    {
        return $this->reviewedEntityId;
    }

    public function setReviewedEntityId(Uuid $reviewedEntityId): self
    {
        $this->reviewedEntityId = $reviewedEntityId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getOriginEntityClass(): ?string
    {
        return $this->originEntityClass;
    }

    /**
     * @param string|null $originEntityClass
     * @return Review
     */
    public function setOriginEntityClass(?string $originEntityClass): Review
    {
        $this->originEntityClass = $originEntityClass;
        return $this;
    }

    /**
     * @return Uuid|null
     */
    public function getOriginEntityId(): ?Uuid
    {
        return $this->originEntityId;
    }

    /**
     * @param Uuid|null $originEntityId
     * @return Review
     */
    public function setOriginEntityId(?Uuid $originEntityId): Review
    {
        $this->originEntityId = $originEntityId;
        return $this;
    }
}
