<?php

namespace App\Entity;

use ApiPlatform\Metadata\Link;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use App\State\Permission\PermissionCollectionProvider;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Uid\Uuid;
#[ApiResource(
    operations: [new Get(openapi: false)],
    normalizationContext: ['groups' => ['user']],
    denormalizationContext: ['groups' => ['user']],
    paginationEnabled: false,

)]
#[ApiResource(
    uriTemplate: '/roles/{roleId}/permissions.{_format}',
    operations: [new GetCollection(provider: PermissionCollectionProvider::class)],
    uriVariables: ['roleId' => new Link(fromClass: Role::class, identifiers: ['id'])],
    status: 200,
    normalizationContext: ['groups' => ['user']]
)]
#[ApiResource(
    uriTemplate: '/users/{userId}/permissions.{_format}',
    operations: [new GetCollection(provider: PermissionCollectionProvider::class)],
    uriVariables: ['userId' => new Link(fromClass: User::class, identifiers: ['userId'])],
    status: 200,
    normalizationContext: ['groups' => ['user']]
)]
class Permission
{
    /**
     * only for class to have identifier
     */
    #[ApiProperty(identifier: true)]
    #[ORM\GeneratedValue]
    #[Groups("user")]
    #[SerializedName("id")]
    private ?string $id;
    #[Groups("user")]
    #[SerializedName("permission_name")]
    private ?string $permissionName;
    #[Groups("user")]
    private ?string $description;
    #[Groups("user")]
    #[SerializedName("resource_server_name")]
    private ?string $resourceServerName;
    #[Groups("user")]
    #[SerializedName("resource_server_identifier")]
    private ?string $resourceServerIdentifier;
    #[Groups("user")]
    private ?string $action;
    #[Groups("user")]
    private ?string $subject;
    public function __construct()
    {
        $this->id = Uuid::v4();
    }
    /**
     * @return string|null
     */
    public function getId() : string|null
    {
        return $this->id;
    }
    /**
     * @param string|null $id
     */
    public function setId(string|null $id) : void
    {
        $this->id = $id;
    }
    /**
     * @return string|null
     */
    public function getPermissionName() : ?string
    {
        return $this->permissionName;
    }
    /**
     * @param string|null $permissionName
     */
    public function setPermissionName(?string $permissionName) : void
    {
        [$action, $subject] = explode(':', $permissionName);
        $this->action = $action;
        $this->subject = $subject;
        $this->permissionName = $permissionName;
    }
    /**
     * @return string|null
     */
    public function getDescription() : ?string
    {
        return $this->description;
    }
    /**
     * @param string|null $description
     */
    public function setDescription(?string $description) : void
    {
        $this->description = $description;
    }
    /**
     * @return string|null
     */
    public function getResourceServerName() : ?string
    {
        return $this->resourceServerName;
    }
    /**
     * @param string|null $resourceServerName
     */
    public function setResourceServerName(?string $resourceServerName) : void
    {
        $this->resourceServerName = $resourceServerName;
    }
    /**
     * @return string|null
     */
    public function getResourceServerIdentifier() : ?string
    {
        return $this->resourceServerIdentifier;
    }
    /**
     * @param string|null $resourceServerIdentifier
     */
    public function setResourceServerIdentifier(?string $resourceServerIdentifier) : void
    {
        $this->resourceServerIdentifier = $resourceServerIdentifier;
    }
    /**
     * @return string|null
     */
    public function getAction() : ?string
    {
        return $this->action;
    }
    /**
     * @param string|null $action
     * @return Permission
     */
    public function setAction(?string $action) : Permission
    {
        $this->action = $action;
        return $this;
    }
    /**
     * @return string|null
     */
    public function getSubject() : ?string
    {
        return $this->subject;
    }
    /**
     * @param string|null $subject
     * @return Permission
     */
    public function setSubject(?string $subject) : Permission
    {
        $this->subject = $subject;
        return $this;
    }
}
