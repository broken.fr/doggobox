<?php
namespace App\EventSubscriber;

use ApiPlatform\Symfony\EventListener\EventPriorities;
use App\Entity\Notification;
use App\Entity\WalkSubscription;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class WalkSubscriptionNotificationSubscriber implements EventSubscriberInterface
{
    const WALK_SUBSCRIPTION_ACCEPTED = 'walk_subscription_accepted';
    const WALK_SUBSCRIPTION_REFUSED = 'walk_subscription_refused';
    const WALK_SUBSCRIPTION_PENDING = 'walk_subscription_pending';
    const WALK_SUBSCRIPTION_CREATED = 'walk_subscription_created';
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['createNotification', EventPriorities::POST_WRITE],
        ];
    }

    public function createNotification(ViewEvent $event)
    {
        $request = $event->getRequest();
        $method = $request->getMethod();

        $walkSubscription = $event->getControllerResult();
        /** @var WalkSubscription $previousData */
        $previousData = $request->get('previous_data');
        if (!$walkSubscription instanceof WalkSubscription) {
            if (!$previousData instanceof WalkSubscription) {
                return;
            }
        }


        if ($walkSubscription instanceof WalkSubscription) {
            $acceptedChanged = $previousData instanceof WalkSubscription && $previousData->isAccepted() !== $walkSubscription->isAccepted();
        } else {
            $walkSubscription = $previousData;
            $acceptedChanged = false;
        }

        $notification = new Notification();
        $notification->setType('walk_subscription_updated');

        if ($acceptedChanged) {
            $accepted = $walkSubscription->isAccepted();
            $message = $accepted ? self::WALK_SUBSCRIPTION_ACCEPTED: self::WALK_SUBSCRIPTION_PENDING;
        } else {
            $message = ($method === Request::METHOD_DELETE) ? self::WALK_SUBSCRIPTION_REFUSED : self::WALK_SUBSCRIPTION_CREATED;
        }

        $userExtraData = ($message !== self::WALK_SUBSCRIPTION_CREATED) ?$walkSubscription->getDog()->getUserExtraData() : $walkSubscription->getWalk()->getOwner();
        $notification->setMessage($message)
            ->setRecipient($userExtraData)
            ->setRelatedEntityClass(WalkSubscription::class)
            ->setRelatedEntityId($walkSubscription->getWalk()->getId())
        ;


        $this->entityManager->persist($notification);
        $this->entityManager->flush();
    }
}
