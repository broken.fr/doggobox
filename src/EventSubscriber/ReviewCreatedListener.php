<?php

namespace App\EventSubscriber;

use ApiPlatform\Symfony\EventListener\EventPriorities;
use App\Entity\Review;
use App\Entity\Walk;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ReviewCreatedListener implements EventSubscriberInterface
{

    public function __construct(private readonly EntityManagerInterface $entityManager) {}
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['checkWalkIsFinished', EventPriorities::PRE_WRITE],
        ];
    }

    public function checkWalkIsFinished(ViewEvent $event): void
    {
        $review = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$review instanceof Review || Request::METHOD_POST !== $method || $review->getOriginEntityClass() !== Walk::class) {
            return;
        }

        $walk = $this->entityManager->getRepository(Walk::class)->find($review->getOriginEntityId());
        $walkDate = clone $walk->getWalkDate();
        $duration = $walk->getWalkDuration();
        $endTime = $walkDate->modify("+{$duration} minutes");
        $now = new \DateTime();
        $interval = $now->diff($endTime);
        $isWalkFinished = $interval->invert === 1;
        if (!$isWalkFinished) {
            throw new \InvalidArgumentException('The walk must be finished before submitting a review');
        }
    }
}