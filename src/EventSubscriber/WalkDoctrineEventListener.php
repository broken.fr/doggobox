<?php

namespace App\EventSubscriber;

use App\Entity\Walk;
use App\Message\WalkDelayedMessage;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Event\PostPersistEventArgs;
use Doctrine\ORM\Event\PostRemoveEventArgs;
use Doctrine\ORM\Event\PostUpdateEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DelayStamp;

#[AsEntityListener(event: Events::postUpdate, method: 'dispatchWalkDelayedMessage', entity: Walk::class)]
#[AsEntityListener(event: Events::postPersist, method: 'dispatchWalkDelayedMessage', entity: Walk::class)]
#[AsEntityListener(event: Events::postRemove, method: 'cancelWalkDelayedMessage', entity: Walk::class)]
class WalkDoctrineEventListener
{

    public function __construct(private MessageBusInterface $bus)
    {
    }

    public function dispatchWalkDelayedMessage(Walk $walk, PostPersistEventArgs|PostUpdateEventArgs $args): void
    {
        $walkDate = clone $walk->getWalkDate();
        $duration = $walk->getWalkDuration();
        $endTime = $walkDate->modify("+{$duration} minutes");

        $this->bus->dispatch(
            new WalkDelayedMessage($walk),
            [DelayStamp::delayUntil($endTime)]
        );

    }

    public function cancelWalkDelayedMessage(Walk $walk, PostRemoveEventArgs $args): void
    {
        dump($walk, $args);

        // TODO: implement logic to cancel delayed message
    }
}