<?php

namespace App\EventSubscriber;

use ApiPlatform\Symfony\EventListener\EventPriorities;
use App\Entity\UserExtraData;
use App\Service\Auth0Manager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class UserExtraDataSubscriber implements EventSubscriberInterface
{
    public function __construct(private Auth0Manager $auth0Manager) {}
    public function addUserRoles(ViewEvent $event): void
    {
        $request = $event->getRequest();
        $method = $request->getMethod();
        if ($method !== Request::METHOD_POST) {
            return;
        }
        $userExtraData = $event->getControllerResult();
        if (!$userExtraData instanceof UserExtraData) {
            return;
        }
        $roles = json_decode($this->auth0Manager->management()->roles()->getAll()->getBody()->getContents());
        foreach($roles as $role) {
            if ($role->name === 'Utilisateur') {
                $userId = str_replace('/api/users/', '', $userExtraData->getUser());
                $this->auth0Manager->management()->roles()->addUsers($role->id, [$userId]);
            }
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['addUserRoles', EventPriorities::POST_WRITE],
        ];
    }
}
