<?php

namespace App\EventSubscriber;

use App\Entity\Walk;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;

#[AsEntityListener(event: Events::preUpdate, method: 'checkWalkDate', entity: Walk::class)]
class WalkFinishedSubscriber
{
    public function checkWalkDate(Walk $walk, PreUpdateEventArgs $args): void
    {

        try {
            $walkDate = $args->getOldValue('walkDate');
        } catch (\InvalidArgumentException $e) {
            $walkDate = $walk->getWalkDate();
        }
        if ($walkDate < new \DateTime()) {
            throw new \Exception('Cannot modify a walk with a past date.');
        }

    }
}
