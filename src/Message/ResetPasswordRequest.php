<?php

namespace App\Message;

use Symfony\Component\Serializer\Annotation\Groups;

final class ResetPasswordRequest
{
    #[Groups(["user", "user_create", "user_update"])]
    public string $email;

    public function __construct(...$args)
    {
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return ResetPasswordRequest
     */
    public function setEmail(string $email): ResetPasswordRequest
    {
        $this->email = $email;
        return $this;
    }


}