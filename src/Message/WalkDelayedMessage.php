<?php

namespace App\Message;

use App\Entity\Walk;

class WalkDelayedMessage
{
    public function __construct (private Walk $walk) {}

    /**
     * @return Walk
     */
    public function getWalk(): Walk
    {
        return $this->walk;
    }

    /**
     * @param Walk $walk
     * @return WalkDelayedMessage
     */
    public function setWalk(Walk $walk): WalkDelayedMessage
    {
        $this->walk = $walk;
        return $this;
    }


}