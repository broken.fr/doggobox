<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220916212516 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE dog ADD dog_adventurous_temper TINYINT(1) NOT NULL, ADD dog_grumpy_temper TINYINT(1) NOT NULL, ADD dog_peaceful_temper TINYINT(1) NOT NULL, ADD dog_brawler_temper TINYINT(1) NOT NULL, ADD dog_independent_temper TINYINT(1) NOT NULL, ADD dog_brutal_temper TINYINT(1) NOT NULL, ADD dog_mild_temper TINYINT(1) NOT NULL, ADD dog_shy_temper TINYINT(1) NOT NULL, ADD dog_sniff INT NOT NULL, ADD dog_to_swim INT NOT NULL, ADD dog_to_fight INT NOT NULL, ADD dog_energy_level INT NOT NULL, DROP dog_degree_peacefullness, DROP dog_degree_runawayness, DROP dog_degree_excitement, CHANGE dog_category dog_category VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE dog ADD dog_degree_peacefullness INT NOT NULL, ADD dog_degree_runawayness INT NOT NULL, ADD dog_degree_excitement INT NOT NULL, DROP dog_adventurous_temper, DROP dog_grumpy_temper, DROP dog_peaceful_temper, DROP dog_brawler_temper, DROP dog_independent_temper, DROP dog_brutal_temper, DROP dog_mild_temper, DROP dog_shy_temper, DROP dog_sniff, DROP dog_to_swim, DROP dog_to_fight, DROP dog_energy_level, CHANGE dog_category dog_category VARCHAR(255) DEFAULT NULL');
    }
}
