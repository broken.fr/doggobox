<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221220203124 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE dog ADD dog_sniffer INT NOT NULL, ADD dog_swimmer INT NOT NULL, ADD dog_fighter INT NOT NULL, DROP dog_sniff, DROP dog_to_swim, DROP dog_to_fight');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE dog ADD dog_sniff INT NOT NULL, ADD dog_to_swim INT NOT NULL, ADD dog_to_fight INT NOT NULL, DROP dog_sniffer, DROP dog_swimmer, DROP dog_fighter');
    }
}
