<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220819062406 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE dog (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', breed_id INT DEFAULT NULL, dog_name VARCHAR(255) NOT NULL, dog_sterilized TINYINT(1) NOT NULL, dog_gender VARCHAR(255) NOT NULL, dog_birthdate DATETIME NOT NULL, dog_category VARCHAR(255) NOT NULL, dog_amity_males INT NOT NULL, dog_amity_females INT NOT NULL, dog_amityPuppies INT NOT NULL, dog_degree_peacefullness INT NOT NULL, dog_degree_runawayness INT NOT NULL, dog_degree_excitement INT NOT NULL, dog_playfullness INT NOT NULL, dog_sharing INT NOT NULL, dog_runner INT NOT NULL, dog_return_response INT NOT NULL, dog_stop_response INT NOT NULL, dog_resume LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_812C397DA8B4A30F (breed_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE dog ADD CONSTRAINT FK_812C397DA8B4A30F FOREIGN KEY (breed_id) REFERENCES breed (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE dog DROP FOREIGN KEY FK_812C397DA8B4A30F');
        $this->addSql('DROP TABLE dog');
    }
}
