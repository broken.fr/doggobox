<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220820190640 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user_extra_data (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', travel_area INT NOT NULL, positive_way INT NOT NULL, natural_way INT NOT NULL, traditional_way INT NOT NULL, resume LONGTEXT DEFAULT NULL, user VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE dog ADD user_extra_data_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', DROP user');
        $this->addSql('ALTER TABLE dog ADD CONSTRAINT FK_812C397D1A00B8E FOREIGN KEY (user_extra_data_id) REFERENCES user_extra_data (id)');
        $this->addSql('CREATE INDEX IDX_812C397D1A00B8E ON dog (user_extra_data_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE dog DROP FOREIGN KEY FK_812C397D1A00B8E');
        $this->addSql('DROP TABLE user_extra_data');
        $this->addSql('DROP INDEX IDX_812C397D1A00B8E ON dog');
        $this->addSql('ALTER TABLE dog ADD user VARCHAR(255) NOT NULL, DROP user_extra_data_id');
    }
}
