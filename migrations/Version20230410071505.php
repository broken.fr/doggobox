<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230410071505 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE review (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', reviewer_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', score INT NOT NULL, type VARCHAR(255) NOT NULL, related_entity_class VARCHAR(255) NOT NULL, related_entity_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', reviewed_entity_class VARCHAR(255) NOT NULL, reviewed_entity_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_794381C670574616 (reviewer_id), INDEX related_entity_id_idx (related_entity_id), INDEX related_entity_class_idx (related_entity_class), INDEX reviewed_entity_id_idx (reviewed_entity_id), INDEX reviewed_entity_class_idx (reviewed_entity_class), INDEX type (type), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE review ADD CONSTRAINT FK_794381C670574616 FOREIGN KEY (reviewer_id) REFERENCES user_extra_data (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE review DROP FOREIGN KEY FK_794381C670574616');
        $this->addSql('DROP TABLE review');
    }
}
