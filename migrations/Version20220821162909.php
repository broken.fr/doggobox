<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220821162909 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE dog CHANGE user_extra_data_id user_extra_data_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', CHANGE dog_category dog_category VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE dog CHANGE user_extra_data_id user_extra_data_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', CHANGE dog_category dog_category VARCHAR(255) NOT NULL');
    }
}
