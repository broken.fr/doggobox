<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230315192756 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE walk (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', owner_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', longitude DOUBLE PRECISION NOT NULL, latitude DOUBLE PRECISION NOT NULL, description VARCHAR(255) DEFAULT NULL, participants_max INT NOT NULL, walk_duration INT DEFAULT NULL, walk_date DATETIME NOT NULL, walk_type VARCHAR(255) DEFAULT NULL, rules_acceptance TINYINT(1) NOT NULL, INDEX IDX_8D917A557E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE walk_subscription (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', dog_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', walk_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', accepted TINYINT(1) NOT NULL, INDEX IDX_282747A8634DFEB (dog_id), INDEX IDX_282747A85EEE1B48 (walk_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE walk ADD CONSTRAINT FK_8D917A557E3C61F9 FOREIGN KEY (owner_id) REFERENCES user_extra_data (id)');
        $this->addSql('ALTER TABLE walk_subscription ADD CONSTRAINT FK_282747A8634DFEB FOREIGN KEY (dog_id) REFERENCES dog (id)');
        $this->addSql('ALTER TABLE walk_subscription ADD CONSTRAINT FK_282747A85EEE1B48 FOREIGN KEY (walk_id) REFERENCES walk (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE walk DROP FOREIGN KEY FK_8D917A557E3C61F9');
        $this->addSql('ALTER TABLE walk_subscription DROP FOREIGN KEY FK_282747A8634DFEB');
        $this->addSql('ALTER TABLE walk_subscription DROP FOREIGN KEY FK_282747A85EEE1B48');
        $this->addSql('DROP TABLE walk');
        $this->addSql('DROP TABLE walk_subscription');
    }
}
