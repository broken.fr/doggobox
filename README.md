Doggobox
========

Installation
------------

Php redis is required : https://windows.php.net/downloads/pecl/releases/redis/5.3.7/php_redis-5.3.7-8.1-ts-vs16-x64.zip 
```bash
    composer install
    php bin/console doctrine:migrations:migrate
    docker run -p 6379:6379 --name redis --net host -e REDIS_PASSWORD=redis -d redis /bin/sh -c 'redis-server --appendonly yes --requirepass ${REDIS_PASSWORD}' --save 60 1 --loglevel warning 
    docker run -p 6379:6379 --name redis -d redis redis-server --save 60 1 --loglevel warning
    docker run -p 6379:6379 --name redis -d redis:5.0.12 
```
### Rights
````bash
HTTPDUSER=$(ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1)
sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var
sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var
HTTPDUSER=$(ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1)
sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX public/media
sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX public/media
````
Running docker-compose : 

create a .env.local file : 
```dotenv
DATABASE_URL="mysql://root:admin@database:3306/doggobox?serverVersion=8.0&charset=utf8mb4"

CORS_ALLOW_ORIGIN='*'

AUTH0_AUDIENCE=<redacted>
AUTH0_DOMAIN=<redacted>
AUTH0_CLIENT_ID=<redacted>
AUTH0_CLIENT_SECRET=<redacted>
AUTH0_MANAGEMENT_AUDIENCE=<redacted>
AUTH0_MANAGEMENT_DOMAIN=<redacted>
AUTH0_CONNECTION=<redacted>

REDIS_HOST=redis
REDIS_PORT=6379
REDIS_PASSWORD=redis
REDIS_DBINDEX=3
```

```bash
    composer install --no-scripts
    docker-compose up -d --build
```


Troubleshoot
------------
1 - cURL error 60: SSL certificate problem: unable to get local issuer certificate 
https://auth0.com/docs/libraries/auth0-php/troubleshoot-auth0-php-library 

Postman Collection
------------------

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/3366460-d5537ad0-6a16-40ab-9121-9d5e1bcbe99e?action=collection%2Ffork&collection-url=entityId%3D3366460-d5537ad0-6a16-40ab-9121-9d5e1bcbe99e%26entityType%3Dcollection%26workspaceId%3D3cf2320c-7d30-48e4-b6ce-a2a45ee0eded#?env%5BDoggobox%20prod%5D=W3sia2V5IjoidXJsIiwidmFsdWUiOiJodHRwOi8vZG9nZ29ib3guYnJva2VuLmZyIiwiZW5hYmxlZCI6dHJ1ZSwidHlwZSI6ImRlZmF1bHQiLCJzZXNzaW9uVmFsdWUiOiJodHRwOi8vZG9nZ29ib3gubG9jIiwic2Vzc2lvbkluZGV4IjowfV0=)
