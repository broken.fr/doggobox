# Adapted from https://github.com/dunglas/symfony-docker


# Prod image
FROM caddy:2-alpine AS caddy_upstream
FROM php:8.2-fpm-alpine AS app_php


WORKDIR /srv/app

## persistent / runtime deps
RUN apk add --no-cache \
		acl \
		fcgi \
		file \
		gettext \
		git \
        curl \
	;

RUN set -eux; \
	apk add --no-cache --virtual .build-deps \
        pcre-dev \
		$PHPIZE_DEPS \
		icu-data-full \
		icu-dev \
		libzip-dev \
		zlib-dev \
	; \
	\
	docker-php-ext-configure zip; \
	docker-php-ext-install -j$(nproc) \
		intl \
		zip \
        pdo \
        pdo_mysql \
	; \
	pecl install \
		apcu \
        redis \
	; \
	pecl clear-cache; \
	docker-php-ext-enable \
		apcu \
        redis.so \
		opcache \
	; \
	\
	runDeps="$( \
		scanelf --needed --nobanner --format '%n#p' --recursive /usr/local/lib/php/extensions \
			| tr ',' '\n' \
			| sort -u \
			| awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
	)"; \
	apk add --no-cache --virtual .app-phpexts-rundeps $runDeps; \
	\
	apk del .build-deps


#RUN curl -L "https://github.com/elastic/apm-agent-php/releases/download/v1.8.3/apm-agent-php_1.8.3_all.apk" > /tmp/apm-agent-php.apk && \
#     apk add --allow-untrusted /tmp/apm-agent-php.apk && \
#     rm /tmp/apm-agent-php.apk

###> recipes ###
###> doctrine/doctrine-bundle ###
#RUN apk add --no-cache --virtual .pgsql-deps postgresql-dev; \
#	docker-php-ext-install -j$(nproc) pdo_pgsql; \
#	apk add --no-cache --virtual .pgsql-rundeps so:libpq.so.5; \
#	apk del .pgsql-deps
###< doctrine/doctrine-bundle ###
###< recipes ###

ENV APP_ENV=prod
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
COPY docker/php/conf.d/app.ini $PHP_INI_DIR/conf.d/
COPY docker/php/conf.d/app.prod.ini $PHP_INI_DIR/conf.d/

COPY docker/php/php-fpm.d/zz-docker.conf /usr/local/etc/php-fpm.d/zz-docker.conf
#COPY docker/php/php-fpm.d/99-elastic-apm-custom.ini /usr/local/etc/php/conf.d/99-elastic-apm-custom.ini

RUN mkdir -p /var/run/php

COPY docker/php/docker-healthcheck.sh /usr/local/bin/docker-healthcheck
RUN chmod +x /usr/local/bin/docker-healthcheck

HEALTHCHECK --interval=10s --timeout=3s --retries=3 CMD ["docker-healthcheck"]

COPY docker/php/docker-entrypoint.sh /usr/local/bin/docker-entrypoint

RUN chmod +x /usr/local/bin/docker-entrypoint


ENTRYPOINT ["docker-entrypoint"]
CMD ["php-fpm"]

# https://getcomposer.org/doc/03-cli.md#composer-allow-superuser
ENV COMPOSER_ALLOW_SUPERUSER=1
ENV PATH="${PATH}:/root/.composer/vendor/bin"

COPY --from=composer:2 /usr/bin/composer /usr/bin/composer

# prevent the reinstallation of vendors at every changes in the source code
COPY composer.* symfony.* ./
RUN set -eux; \
	composer install --prefer-dist --no-dev --no-autoloader --no-scripts --no-progress; \
	composer clear-cache

# copy sources
COPY . .
RUN rm -Rf docker/

RUN set -eux; \
	mkdir -p var/cache var/log; \
	composer dump-autoload --classmap-authoritative --no-dev; \
#	composer dump-env prod; \
#	composer run-script --no-dev post-install-cmd; \
	chmod +x bin/console; sync

# RUN if [ "$ENV" = "prod" ] ; then composer run-script --no-dev post-install-cmd; fi

# Dev image
FROM app_php AS app_php_dev

ENV APP_ENV=dev XDEBUG_MODE=off
#VOLUME /srv/app/var/
#VOLUME /srv/app/vendor/

RUN rm $PHP_INI_DIR/conf.d/app.prod.ini; \
	mv "$PHP_INI_DIR/php.ini" "$PHP_INI_DIR/php.ini-production"; \
	mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

COPY docker/php/conf.d/app.dev.ini $PHP_INI_DIR/conf.d/

RUN set -eux; \
    apk add --update linux-headers; \
	apk add --no-cache --virtual .build-deps $PHPIZE_DEPS pcre-dev; \
	pecl install xdebug redis; \
    docker-php-ext-enable xdebug; \
	apk del .build-deps
COPY ./vendor ./vendor

#RUN rm -f .env.local.php


# Base Caddy image
FROM caddy_upstream AS caddy_base

ARG TARGETARCH

WORKDIR /srv/app

# Download Caddy compiled with the Mercure and Vulcain modules
ADD --chmod=500 https://caddyserver.com/api/download?os=linux&arch=$TARGETARCH&p=github.com/dunglas/mercure/caddy&p=github.com/dunglas/vulcain/caddy /usr/bin/caddy

COPY --link docker/caddy/Caddyfile /etc/caddy/Caddyfile

# Prod Caddy image
FROM caddy_base AS caddy_prod

COPY --from=app_php --link /srv/app/public public/
